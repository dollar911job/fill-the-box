﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Game.Controllers;
using Game.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace Game.Entities
{
	public class BoxSpawnerEntity
	{
		private readonly BoxController.Factory _boxFactory;
		private readonly BoxSpawnerScriptableObject _boxSpawnerSObj;

		public int Width => _boxSpawnerSObj.Width;

		private readonly int _allBoxesCount;

		public BoxSpawnerEntity(
			BoxController.Factory boxFactory,
			BoxSpawnerScriptableObject boxSpawnerSObj)
		{
			_boxFactory = boxFactory;
			_boxSpawnerSObj = boxSpawnerSObj;

			_allBoxesCount = _boxSpawnerSObj.ContentBoxes.Length + _boxSpawnerSObj.ContainerBoxes.Length;
		}


		public async Task<BoxController[]> Spawn(int contentCellsCount, int containerCellsCount)
		{
			var boxes = new List<BoxController>();
			if(_boxSpawnerSObj.MultiSpawn)
			{
				var space = Random.Range(Width / 2, Width);
				while(true)
				{
					var box = await GetRandomBox(contentCellsCount, containerCellsCount);
					
					if(box.size.x < space)
					{
						boxes.Add(box);
						space -= box.size.x;
						if(box.type == BoxController.Type.Content)
							contentCellsCount += box.cells.Length;
						else
							containerCellsCount += box.cells.Length;
					}
					else
					{
						_boxFactory.Release(box);
						break;
					}
				}
			}
			else
			{
				boxes.Add(await GetRandomBox(contentCellsCount, containerCellsCount));
			}

			return boxes.ToArray();
		}

		private async Task<BoxController> GetRandomBox(int contentCellsCount, int containerCellsCount)
		{
			(BoxScriptableObject boxSObj, BoxController.Type type) boxResult;
			if(contentCellsCount < containerCellsCount && _boxSpawnerSObj.MinAllowBoxesRatio > (float)contentCellsCount / containerCellsCount)
			{
				boxResult = (_boxSpawnerSObj.ContentBoxes[Random.Range(0, _boxSpawnerSObj.ContentBoxes.Length)], BoxController.Type.Content);
			}
			else if(containerCellsCount < contentCellsCount && _boxSpawnerSObj.MinAllowBoxesRatio > (float)containerCellsCount / contentCellsCount)
			{
				boxResult = (_boxSpawnerSObj.ContainerBoxes[Random.Range(0, _boxSpawnerSObj.ContainerBoxes.Length)], BoxController.Type.Container);
			}
			else
			{
				var rnd = Random.Range(0, _allBoxesCount);
				boxResult = rnd < _boxSpawnerSObj.ContentBoxes.Length
					? (_boxSpawnerSObj.ContentBoxes[rnd], BoxController.Type.Content)
					: (_boxSpawnerSObj.ContainerBoxes[rnd - _boxSpawnerSObj.ContentBoxes.Length], BoxController.Type.Container);
			}

			var box = await _boxFactory.Create(boxResult.boxSObj);
			box.SetType(boxResult.type);
			box.Setup();
			return box;
		}

		public class Factory : IFactory<BoxSpawnerScriptableObject, BoxSpawnerEntity>
		{
			private readonly BoxController.Factory _boxFactory;

			public Factory(BoxController.Factory boxFactory)
			{
				_boxFactory = boxFactory;
			}

			public BoxSpawnerEntity Create(BoxSpawnerScriptableObject boxSpawnerSo) => 
				new BoxSpawnerEntity(_boxFactory, boxSpawnerSo);
		}
	}
}
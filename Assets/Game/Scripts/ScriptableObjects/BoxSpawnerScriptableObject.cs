﻿using UnityEngine;

namespace Game.ScriptableObjects
{
	[CreateAssetMenu(fileName = "BoxSpawner", menuName = "ScriptableObjects/BoxSpawner")]
	public class BoxSpawnerScriptableObject : ScriptableObject
	{
		[SerializeField]
		private int _width;
		public int Width => _width;

		[SerializeField]
		private BoxScriptableObject[] _contentBoxes;
		public BoxScriptableObject[] ContentBoxes => _contentBoxes;

		[SerializeField]
		private BoxScriptableObject[] _containerBoxes;
		public BoxScriptableObject[] ContainerBoxes => _containerBoxes;

		[SerializeField]
		private bool _multiSpawn;
		public bool MultiSpawn => _multiSpawn;

		[SerializeField]
		[Range(0f,1f)]
		private float _minAllowBoxesRatio;
		public float MinAllowBoxesRatio => _minAllowBoxesRatio;
	}
}
﻿using System;
using Game.Controllers;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.ScriptableObjects
{
	[CreateAssetMenu(fileName = "Box", menuName = "ScriptableObjects/Box")]
	public class BoxScriptableObject : ScriptableObject
	{
		[SerializeField]
		private AssetReference _boxControllerReference;
		public AssetReference BoxControllerReference => _boxControllerReference;

		[SerializeField]
		private Vector2Int[] _cells;
		public Vector2Int[] Cells => _cells;

		[Serializable]
		public class Settings
		{
			[SerializeField]
			private Color _containerColor;
			public Color ContainerColor => _containerColor;

			[SerializeField]
			private Color _inContainerAdditionalColor;
			public Color InContainerAdditionalColor => _inContainerAdditionalColor;

			[SerializeField]
			private Color _inPreviewAdditionalColor;
			public Color InPreviewAdditionalColor => _inPreviewAdditionalColor;

			[SerializeField]
			private Material _boxMaterial;
			public Material BoxMaterial => _boxMaterial;

			[SerializeField]
			private Material _dragTargetBoxMaterial;
			public Material DragTargetBoxMaterial => _dragTargetBoxMaterial;

			[SerializeField]
			private float _acceleration;
			public float Acceleration => _acceleration;

			[SerializeField]
			private float _boxInFieldPositionZ;
			public float BoxInFieldPositionZ => _boxInFieldPositionZ;

			[SerializeField]
			private float _boxMoveUpPositionZ;
			public float BoxMoveUpPositionZ => _boxMoveUpPositionZ;

			[SerializeField]
			private float _contentInContainerPositionZ;
			public float ContentInContainerPositionZ => _contentInContainerPositionZ;
		}
	}
}
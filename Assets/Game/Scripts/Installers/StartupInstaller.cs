using Game.Controllers.UI;
using Game.Services;
using Game.StateMachine;
using Game.Strategies;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace Game.Installers
{
	public class StartupInstaller : MonoInstaller
	{
		[SerializeField]
		private StartupUiController _startupUiController;

		public override void InstallBindings()
		{
			Container.BindInstance(_startupUiController);
			Container.BindInterfacesAndSelfTo<StartupService>().AsSingle();
			Container.BindInterfacesAndSelfTo<StartupAppStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<ReadyToPlayStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<LoadingGameSceneStrategy>().AsSingle();
		}

		public override void Start()
		{
			Container.Resolve<GameStateMachine>().Start();
			Container.Resolve<SfxService.Factory>().SetParent(Camera.main.transform);
		}
	}
}

﻿using Game.ScriptableObjects;
using Game.Services;
using UnityEngine;
using Zenject;

namespace Game.Installers
{
	[CreateAssetMenu(fileName = "GameSettings", menuName = "ScriptableObjects/Installers/GameSettings")]
	public class GameSettingsInstaller : ScriptableObjectInstaller
	{
		public BoxScriptableObject.Settings box;
		public GameDataService.Settings gameField;
		public ScreenService.Settings screens;
		public VfxService.Settings vfx;
		public SfxService.Settings sfx;

		public override void InstallBindings()
		{
			Container.BindInstances(box, gameField, screens, vfx, sfx);
		}
	}
}
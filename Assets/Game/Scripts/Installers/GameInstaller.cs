﻿using DG.Tweening;
using Game.Controllers;
using Game.Controllers.UI;
using Game.Services;
using Game.StateMachine;
using Game.Strategies;
using UnityEngine;
using Zenject;

namespace Game.Installers
{
	public class GameInstaller : MonoInstaller
	{
		[SerializeField]
		private InputEventController _dragEventController;

		[SerializeField]
		private GameUiController _gameUiController;

		[SerializeField]
		private RectTransform _screensUiRoot;

		[SerializeField]
		private Transform _boxesRoot;

		[SerializeField]
		private RectTransform _vfxRoot;

		[SerializeField]
		private RectTransform _vfxCoinsTargetToMove;

		public override void InstallBindings()
		{
			Container.Bind<InputEventController>().FromInstance(_dragEventController).AsSingle();
			Container.Bind<GameUiController>().FromInstance(_gameUiController).AsSingle();

			Container.BindInterfacesAndSelfTo<GameActions>().AsSingle();
			Container.BindInterfacesAndSelfTo<InputService>().AsSingle();
			Container.BindInterfacesAndSelfTo<CameraService>().AsSingle();
			Container.BindInterfacesAndSelfTo<GameService>().AsSingle();
			Container.BindInterfacesAndSelfTo<GameDataService>().AsSingle();
			Container.BindInterfacesAndSelfTo<ScreenService>().AsSingle();
			Container.BindInterfacesAndSelfTo<VfxService>().AsSingle();
			Container.BindInterfacesAndSelfTo<SfxService>().AsSingle();

			Container.BindInterfacesAndSelfTo<StartGameStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<UpdateGameFieldStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<PlayerTurnStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<UseDestroyBoxBoosterStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<UseAddOneBoxesBoosterStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<PreGameOverStrategy>().AsSingle();
			Container.BindInterfacesAndSelfTo<GameOverStrategy>().AsSingle();

			Container.BindInterfacesAndSelfTo<PreGameOverScreenUiController.Factory>().AsSingle();
			Container.BindInterfacesAndSelfTo<GameOverScreenUiController.Factory>().AsSingle();
		}

		public override void Start()
		{
			DOTween.Init();
			Container.Resolve<CameraService>().FitToWidth();
			Container.Resolve<BoxController.Factory>().SetParent(_boxesRoot);
			Container.Resolve<BoxGroundedDustController.Factory>().SetParent(_boxesRoot);
			Container.Resolve<BoxRemovedDustController.Factory>().SetParent(_boxesRoot);
			Container.Resolve<CoinController.Factory>().SetParent(_vfxRoot);
			Container.Resolve<VfxService>().SetCoinsTargetToMove(_vfxCoinsTargetToMove.position);
			Container.Resolve<PreGameOverScreenUiController.Factory>().SetScreensUiRoot(_screensUiRoot);
			Container.Resolve<GameOverScreenUiController.Factory>().SetScreensUiRoot(_screensUiRoot);
			Container.Resolve<SfxService.Factory>().SetParent(Camera.main.transform);
		}
	}
}
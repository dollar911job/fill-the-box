﻿using Game.Controllers;
using Game.Entities;
using Game.Services;
using Game.StateMachine;
using Zenject;

namespace Game.Installers
{
	public class ProjectContextInstaller : MonoInstaller
	{
		public override void InstallBindings()
		{
			Container.BindInterfacesAndSelfTo<GameStateMachine>().AsSingle();
			
			Container.BindInterfacesAndSelfTo<SceneService>().AsSingle();
			
			Container.BindInterfacesAndSelfTo<BoxSpawnerEntity.Factory>().AsSingle();
			Container.BindInterfacesAndSelfTo<BoxController.Factory>().AsSingle();
			Container.BindInterfacesAndSelfTo<BoxGroundedDustController.Factory>().AsSingle();
			Container.BindInterfacesAndSelfTo<BoxRemovedDustController.Factory>().AsSingle();
			Container.BindInterfacesAndSelfTo<CoinController.Factory>().AsSingle();
			Container.BindInterfacesAndSelfTo<SfxService.Factory>().AsSingle();
		}
	}
}
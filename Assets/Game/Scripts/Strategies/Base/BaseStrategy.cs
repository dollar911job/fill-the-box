﻿namespace Game.Strategies
{
	public interface IBaseStrategy
	{
		void Start();
		void Finish();
	}

	public abstract class BaseStrategy : IBaseStrategy
	{
		public virtual void Start() { }
		public virtual void Finish() { }
	}
}
﻿using Game.Services;
using Game.StateMachine;
using UnityEngine.SceneManagement;

namespace Game.Strategies
{
	public class LoadingGameSceneStrategy : BaseStrategy
	{
		private readonly GameStateMachine _gameStateMachine;
		private readonly SceneService _sceneService;

		public LoadingGameSceneStrategy(
			GameStateMachine gameStateMachine, 
			SceneService sceneService)
		{
			_gameStateMachine = gameStateMachine;
			_sceneService = sceneService;
		}

		public override async void Start()
		{
			await _sceneService.ActivateGameScene();
			_gameStateMachine.NextState(GameState.StartGame);
		}
	}
}
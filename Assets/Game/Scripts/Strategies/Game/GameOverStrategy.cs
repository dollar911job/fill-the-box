﻿using Game.Controllers.UI;
using Game.Services;
using Game.StateMachine;
using UnityEngine;

namespace Game.Strategies
{
	public sealed class GameOverStrategy : BaseStrategy
	{
		private const string _bestKey = "BestScore";

		private readonly ScreenService _screenService;
		private readonly GameStateMachine _gameStateMachine;
		private readonly GameDataService _gameDataService;

		private GameOverScreenUiController _gameOverScreen;

		public GameOverStrategy(
			ScreenService screenService, 
			GameStateMachine gameStateMachine,
			GameDataService gameDataService)
		{
			_screenService = screenService;
			_gameStateMachine = gameStateMachine;
			_gameDataService = gameDataService;
		}

		public override async void Start()
		{
			_gameOverScreen = await _screenService.GetGameOverScreen();
			_gameOverScreen.PlayAgain += OnPlayAgain;
			var best = PlayerPrefs.GetInt(_bestKey, 0);
			var newRecord = false;
			if(_gameDataService.score.value > best)
			{
				best = _gameDataService.score.value;
				newRecord = true;
				PlayerPrefs.SetInt(_bestKey, best);
			}

			_gameOverScreen.Show(best, _gameDataService.score.value, newRecord);
		}

		private void OnPlayAgain()
		{
			_gameOverScreen.PlayAgain -= OnPlayAgain;
			_gameOverScreen.Hide();
			_gameStateMachine.NextState(GameState.StartGame);
		}
	}
}
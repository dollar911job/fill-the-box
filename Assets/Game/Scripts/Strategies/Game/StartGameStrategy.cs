﻿using Game.Controllers;
using Game.Services;
using Game.StateMachine;

namespace Game.Strategies
{
	public sealed class StartGameStrategy : BaseStrategy
	{
		private readonly BoxController.Factory _boxFactory;
		private readonly GameDataService _gameDataService;
		private readonly GameStateMachine _gameStateMachine;

		public StartGameStrategy(
			BoxController.Factory boxFactory, 
			GameDataService gameDataService,
			GameStateMachine gameStateMachine)
		{
			_boxFactory = boxFactory;
			_gameDataService = gameDataService;
			_gameStateMachine = gameStateMachine;
		}

		public override void Start()
		{
			_boxFactory.ClearPools();
			_gameDataService.Clear();
			_gameStateMachine.NextState(GameState.UpdateGameField);
		}
	}
}
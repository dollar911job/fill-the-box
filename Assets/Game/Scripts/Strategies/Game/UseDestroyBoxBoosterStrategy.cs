﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using Game.Controllers;
using Game.Controllers.UI;
using Game.Services;
using Game.StateMachine;
using UnityEngine;

namespace Game.Strategies
{
	public sealed class UseDestroyBoxBoosterStrategy : BaseStrategy
	{
		private readonly GameDataService _gameDataService;
		private readonly GameDataService.Settings _gameSettings;
		private readonly InputService _inputService;
		private readonly BoxController.Factory _boxFactory;
		private readonly GameStateMachine _gameStateMachine;
		private readonly GameUiController _gameUiController;
		private readonly VfxService _vfxService;
		private readonly SfxService _sfxService;

		private List<BoxController> _shackedBoxes;

		public UseDestroyBoxBoosterStrategy(
			GameDataService gameDataService, 
			GameDataService.Settings gameSettings,
			InputService inputService,
			BoxController.Factory boxFactory,
			GameStateMachine gameStateMachine,
			GameUiController gameUiController,
			VfxService vfxService,
			SfxService sfxService)
		{
			_gameDataService = gameDataService;
			_gameSettings = gameSettings;
			_inputService = inputService;
			_boxFactory = boxFactory;
			_gameStateMachine = gameStateMachine;
			_gameUiController = gameUiController;
			_vfxService = vfxService;
			_sfxService = sfxService;
		}

		public override void Start()
		{
			SubscribeEvens();
			_gameUiController.ToggleDestroyBoxButton(false);
			_shackedBoxes = new();
			foreach(var box in _gameDataService.boxes)
			{
				if(box.Key.type == BoxController.Type.Content && _gameDataService.contentInContainerBoxes.ContainsKey(box.Key) ||
					box.Value.y + box.Key.size.y > _gameSettings.gameZoneHeight)
					continue;

				box.Key.SetShaking(true);
				_shackedBoxes.Add(box.Key);
			}
		}

		public override void Finish()
		{
			foreach(var box in _shackedBoxes)
				box.SetShaking(false);
			_shackedBoxes.Clear();
			UnsubscribeEvens();
		}

		private void OnBoxPressed(BoxController box)
		{
			if(_shackedBoxes.Contains(box))
			{
				UnsubscribeEvens();
				foreach(var content in _gameDataService.contentInContainerBoxes.Keys.ToArray())
				{
					if(_gameDataService.contentInContainerBoxes[content].Item1 == box)
					{
						content.transform.parent = box.transform.parent;
						_boxFactory.Release(content);
						_gameDataService.contentInContainerBoxes.Remove(content);
						_gameDataService.boxes.Remove(content);
					}
				}

				var removedCells = new Vector2Int[box.cells.Length];
				for(var n = 0; n < box.cells.Length; n++)
					removedCells[n] = box.cells[n] + _gameDataService.boxes[box];
				_vfxService.ShowBoxRemovedDust(removedCells);
				_sfxService.PlayAsync(SfxService.Sound.BoxGroundedLow);

				_boxFactory.Release(box);
				_gameDataService.boxes.Remove(box);
				_gameStateMachine.NextState(GameState.UpdateGameField);
			}
		}

		private void OnCancelDestroyBox()
		{
			UnsubscribeEvens();
			_gameStateMachine.NextState(GameState.UpdateGameField);
		}

		private void SubscribeEvens()
		{
			_inputService.Pressed += OnBoxPressed;
			_gameUiController.CancelDestroyBox += OnCancelDestroyBox;
		}

		private void UnsubscribeEvens()
		{
			_inputService.Pressed -= OnBoxPressed;
			_gameUiController.CancelDestroyBox -= OnCancelDestroyBox;
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Game.Controllers;
using Game.Controllers.UI;
using Game.ScriptableObjects;
using Game.Services;
using Game.StateMachine;
using UnityEngine;

namespace Game.Strategies
{
	public sealed class UpdateGameFieldStrategy : BaseStrategy
	{
		private readonly GameDataService _gameDataService;
		private readonly GameDataService.Settings _gameSettings;
		private readonly BoxScriptableObject.Settings _boxSettings;
		private readonly GameStateMachine _gameStateMachine;
		private readonly VfxService _vfxService;
		private readonly VfxService.Settings _vfxSettings;
		private readonly SfxService _sfxService;
		private readonly SfxService.Settings _sfxSettings;
		private readonly GameUiController _gameUiController;

		private Dictionary<BoxController, Vector2Int> _boxesDestination = new();
		private List<Task> _moveTasks = new ();

		public UpdateGameFieldStrategy(
			GameDataService gameDataService,
			GameDataService.Settings gameSettings,
			BoxScriptableObject.Settings boxSettings,
			GameStateMachine gameStateMachine,
			VfxService vfxService,
			VfxService.Settings vfxSettings,
			SfxService sfxService,
			SfxService.Settings sfxSettings,
			GameUiController gameUiController)
		{
			_gameDataService = gameDataService;
			_gameSettings = gameSettings;
			_boxSettings = boxSettings;
			_gameStateMachine = gameStateMachine;
			_vfxService = vfxService;
			_vfxSettings = vfxSettings;
			_sfxService = sfxService;
			_sfxSettings = sfxSettings;
			_gameUiController = gameUiController;
		}

		public override async void Start()
		{
			_boxesDestination.Clear();
			_moveTasks.Clear();
			MoveBoxes(_gameDataService.boxes.Keys.ToList());
			do
			{
				var spawnedBoxes = await TrySpawnBoxes();
				MoveBoxes(spawnedBoxes);
				await Task.Yield();
			} while(!IsMoveTasksCompleted());

			_gameUiController.ToggleSurrenderButton(!IsAvailableMoves());
			_gameStateMachine.NextState(GameState.PlayerTurn);
		}

		private bool IsMoveTasksCompleted()
		{
			foreach(var task in _moveTasks)
			{
				if(!task.IsCompleted)
					return false;
			}
			return true;
		}

		private async Task<List<BoxController>> TrySpawnBoxes()
		{
			var result = new List<BoxController>();
			var x = 0;
			var y = _gameSettings.gameZoneHeight + _gameSettings.previewZoneHeight;
			var height = _gameSettings.spawnZoneHeight;
			foreach(var boxSpawner in _gameDataService.boxSpawners)
			{
				var width = boxSpawner.Width;
				if(_gameDataService.CheckForVoid(new RectInt(x, y, width, height)))
				{
					var contentCellsCount = 0;
					var containerCellsCount = 0;
					foreach(var box in _gameDataService.boxes.Keys)
					{
						if(box.type == BoxController.Type.Content)
							contentCellsCount += box.cells.Length;
						else
							containerCellsCount += box.cells.Length;
					}

					var boxes = await boxSpawner.Spawn(contentCellsCount, containerCellsCount);
					if(boxes.Length == 0)
						continue;

					var allBoxesWidth = 0;
					foreach(var box in boxes)
						allBoxesWidth += box.size.x;
					var freeSpace = width - allBoxesWidth;
					var boxesOffsets = new Dictionary<BoxController, int>();
					var maxOffsetPerBox = Mathf.FloorToInt((float)freeSpace / boxes.Length);
					foreach(var box in boxes)
					{
						var randomOffset = Random.Range(0, maxOffsetPerBox + 1);
						boxesOffsets.Add(box, randomOffset);
						freeSpace -= randomOffset;
					}
					var rightOffset = Random.Range(0, maxOffsetPerBox + 1);
					while(freeSpace > rightOffset)
					{
						var box = boxes[Random.Range(0, boxes.Length)];
						boxesOffsets[box]++;
						freeSpace--;
					}

					var startPosition = 0;
					foreach(var box in boxes)
					{
						var cellsOffset = new Vector2Int(startPosition + boxesOffsets[box], 0);
						box.transform.position = new Vector2(x - _gameDataService.width / 2f + box.size.x / 2f, y + box.size.y / 2f) + cellsOffset;
						_gameDataService.boxes.Add(box, new Vector2Int(x, y) + cellsOffset);
						startPosition += boxesOffsets[box] + box.size.x;
					}
					result.AddRange(boxes);
				}
				x += width;
			}
			return result;
		}

		private void MoveBoxes(List<BoxController> boxes)
		{
			if(boxes.Count == 0)
				return;

			var boxesInCells = new Dictionary<Vector2Int, BoxController>();
			foreach(var box in _boxesDestination)
			{
				foreach(var localCell in box.Key.cells)
					boxesInCells.Add(localCell + box.Value, box.Key);
			}

			foreach(var boxController in boxes)
			{
				if(_gameDataService.contentInContainerBoxes.ContainsKey(boxController))
					continue;

				foreach(var localCell in boxController.cells)
					boxesInCells.Add(localCell + _gameDataService.boxes[boxController], boxController);
			}

			var height = _gameSettings.gameZoneHeight + _gameSettings.previewZoneHeight + _gameSettings.spawnZoneHeight;
			for(var y = 0; y < height; y++)
			{
				for(var x = 0; x < _gameDataService.width; x++)
				{
					var cell = new Vector2Int(x, y);
					if(boxesInCells.ContainsKey(cell) && !_boxesDestination.ContainsKey(boxesInCells[cell]))
					{
						var boxController = boxesInCells[cell];
						var moveDistance = _gameDataService.boxes[boxController].y;
						foreach(var localDownCell in boxController.downCells)
						{
							var downCell = localDownCell + _gameDataService.boxes[boxController];
							for(var cellY = downCell.y - 1; cellY >= 0; cellY--)
							{
								if(boxesInCells.ContainsKey(new Vector2Int(downCell.x, cellY)))
								{
									var distance = downCell.y - cellY - 1;
									if(moveDistance > distance)
										moveDistance = distance;
									break;
								}
							}
						}
						_boxesDestination[boxController] = _gameDataService.boxes[boxController] - new Vector2Int(0, moveDistance);
						boxesInCells = new Dictionary<Vector2Int, BoxController>(boxesInCells.Where((kvp) => kvp.Value != boxController));
						foreach(var localCell in boxController.cells)
							boxesInCells.Add(localCell + _boxesDestination[boxController], boxController);
						_moveTasks.Add( MoveBox(boxController));
					}
				}
			}

			List<Vector2Int> GetOccupiedCells(BoxController boxController)
			{
				var occupiedCells = new List<Vector2Int>();
				foreach(var boxPosition in _boxesDestination)
				{
					if(boxPosition.Key == boxController || _gameDataService.contentInContainerBoxes.ContainsKey(boxPosition.Key))
						continue;

					foreach(var localCell in boxPosition.Key.cells)
						occupiedCells.Add(localCell + boxPosition.Value);
				}
				return occupiedCells;
			}

			async Task MoveBox(BoxController boxController)
			{
				var startCell = _gameDataService.boxes[boxController];
				var startPosition =  new Vector3(startCell.x - _gameDataService.width / 2f + boxController.size.x / 2f, startCell.y + boxController.size.y / 2f, _boxSettings.BoxInFieldPositionZ);
				var finishPosition = new Vector3(startPosition.x, _boxesDestination[boxController].y + boxController.size.y / 2f, startPosition.z);

				var startTime = Time.time;
				while(boxController.transform.position.y > finishPosition.y)
				{
					var time = Time.time - startTime;
					var dist = _boxSettings.Acceleration * time * time / 2f;
					var position = startPosition - new Vector3(0f, dist, 0f);
					if(position.y < finishPosition.y)
						position = finishPosition;
					boxController.transform.position = position;
					if(_gameDataService.boxes.ContainsKey(boxController))
					{
						_gameDataService.boxes[boxController] = startCell - new Vector2Int(0, Mathf.FloorToInt(dist));
						boxController.SetInPreview(_gameDataService.boxes[boxController].y + boxController.size.y > _gameSettings.gameZoneHeight);
						boxController.UpdateColor();
					}
					await Task.Yield();
				}
				var velocity = (Time.time - startTime) * _boxSettings.Acceleration;
				var volume = Mathf.Clamp01(velocity * _sfxSettings.VolumeVelocityCoefficient);
				if(velocity >= _vfxSettings.MinBoxVelocityForGroundedDust)
				{
					_vfxService.ShowBoxGroundedDust(GetGroundedCells(boxController));
					_sfxService.PlayAsync(SfxService.Sound.BoxGroundedHight, volume);
				}
				else
					_sfxService.PlayAsync(SfxService.Sound.BoxGroundedLow, volume);

				_gameDataService.boxes[boxController] = _boxesDestination[boxController];
			}

			Vector2Int[] GetGroundedCells(BoxController boxController)
			{
				var result = new List<Vector2Int>();
				var occupiedCells = GetOccupiedCells(boxController);
				foreach(var localDownCell in boxController.downCells)
				{
					var downCell = localDownCell + _gameDataService.boxes[boxController];
					if(downCell.y == 0 || occupiedCells.Contains(downCell + Vector2Int.down))
						result.Add(downCell);
				}
				return result.ToArray();
			}
		}

		private bool IsAvailableMoves()
		{
			var fieldInfo = _gameDataService.GetFieldInfo();
			foreach(var box in _gameDataService.boxes)
			{
				if(box.Key.type == BoxController.Type.Container || _gameDataService.contentInContainerBoxes.ContainsKey(box.Key))
					continue;

				foreach(var freeCells in fieldInfo.freeContainersCells)
				{
					for(var y = 0; y <= freeCells.Key.size.y - box.Key.size.y; y++)
					{
						for(var x = 0; x <= freeCells.Key.size.x - box.Key.size.x; x++)
						{
							var checkedCells = box.Key.cells.Select((cell) => cell + new Vector2Int(x, y));
							if(freeCells.Value.Intersect(checkedCells).Count() == checkedCells.Count())
								return true;
						}
					}
				}
			}
			return false;
		}
	}
}
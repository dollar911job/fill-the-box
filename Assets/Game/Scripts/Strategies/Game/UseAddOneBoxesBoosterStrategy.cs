﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Game.Controllers;
using Game.Services;
using Game.StateMachine;
using UnityEngine;

namespace Game.Strategies
{
	public sealed class UseAddOneBoxesBoosterStrategy : BaseStrategy
	{
		private static int _oneBoxCount = 3;

		private readonly GameDataService _gameDataService;
		private readonly GameStateMachine _gameStateMachine;
		private readonly BoxController.Factory _boxFactory;
		private readonly GameDataService.Settings _gameSettings;

		public UseAddOneBoxesBoosterStrategy(
			GameDataService gameDataService, 
			GameStateMachine gameStateMachine, 
			BoxController.Factory boxFactory, 
			GameDataService.Settings gameSettings)
		{
			_gameDataService = gameDataService;
			_gameStateMachine = gameStateMachine;
			_boxFactory = boxFactory;
			_gameSettings = gameSettings;
		}

		public override async void Start()
		{
			var freeCells = _gameDataService.GetFieldInfo().freeCells;
			var tasks = new List<Task>();
			if(freeCells.Count >= _oneBoxCount)
			{
				var randomKeys = new List<int>();
				while(randomKeys.Count < _oneBoxCount)
				{
					var rnd = Random.Range(0, freeCells.Count);
					if(!randomKeys.Contains(rnd))
					{
						randomKeys.Add(rnd);
						tasks.Add(SpawnOneBox(freeCells[rnd]));
					}
						
				}
				await Task.WhenAll(tasks);
				_gameStateMachine.NextState(GameState.UpdateGameField);
			}
		}

		private async Task SpawnOneBox(Vector2Int position)
		{
			var box = await _boxFactory.Create(_gameSettings.OneBox);
			box.SetType(BoxController.Type.Content);
			box.Setup();
			box.transform.position = new Vector2(position.x - _gameDataService.width / 2f + box.size.x / 2f, position.y + box.size.y / 2f);
			_gameDataService.boxes.Add(box, position);
		}
	}
}
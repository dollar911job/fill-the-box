﻿using Game.Controllers.UI;
using Game.Services;
using Game.StateMachine;

namespace Game.Strategies
{
	public class ReadyToPlayStrategy : BaseStrategy
	{
		private readonly GameStateMachine _gameStateMachine;
		private readonly StartupUiController _startupUiController;

		public ReadyToPlayStrategy(
			GameStateMachine gameStateMachine, 
			StartupUiController startupUiController)
		{
			_gameStateMachine = gameStateMachine;
			_startupUiController = startupUiController;
		}

		public override void Start()
		{
			_startupUiController.PlayButtonSetActive(true);
			_startupUiController.PlayButtonClicked += OnPlayButtonClicked;
		}

		public override void Finish()
		{
			_startupUiController.PlayButtonSetActive(false);
			_startupUiController.PlayButtonClicked -= OnPlayButtonClicked;
		}

		private void OnPlayButtonClicked()
		{
			_gameStateMachine.NextState(GameState.LoadingGameScene);
		}
	}
}
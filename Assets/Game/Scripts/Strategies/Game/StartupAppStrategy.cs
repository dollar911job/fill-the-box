﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Game.Controllers;
using Game.Controllers.UI;
using Game.ScriptableObjects;
using Game.Services;
using Game.StateMachine;
using UnityEngine;

namespace Game.Strategies
{
	public class StartupAppStrategy : BaseStrategy
	{
		private readonly GameDataService.Settings _gameSettings;
		private readonly BoxController.Factory _boxControllerFactory;
		private readonly BoxGroundedDustController.Factory _boxGroundedDustControllerFactory;
		private readonly BoxRemovedDustController.Factory _boxRemovedDustControllerFactory;
		private readonly CoinController.Factory _coinControllerFactory;
		private readonly SfxService.Factory _sfxFactory;
		private readonly GameStateMachine _gameStateMachine;
		private readonly StartupUiController _startupUiController;

		public StartupAppStrategy(
			GameDataService.Settings gameSettings, 
			BoxController.Factory boxControllerFactory,
			BoxGroundedDustController.Factory boxGroundedDustController,
			BoxRemovedDustController.Factory boxRemovedDustControllerFactory,
			CoinController.Factory coinControllerFactory,
			SfxService.Factory sfxFactory,
			GameStateMachine gameStateMachine,
			StartupUiController startupUiController)
		{
			_gameSettings = gameSettings;
			_boxControllerFactory = boxControllerFactory;
			_boxGroundedDustControllerFactory = boxGroundedDustController;
			_boxRemovedDustControllerFactory = boxRemovedDustControllerFactory;
			_coinControllerFactory = coinControllerFactory;
			_sfxFactory = sfxFactory;
			_gameStateMachine = gameStateMachine;
			_startupUiController = startupUiController;
		}

		public override async void Start()
		{
			var loadingTasks = new List<Task>();
			var boxes = new List<BoxScriptableObject>();
			foreach(var boxSpawnerSObj in _gameSettings.BoxSpawners)
			{
				boxes.AddRange(boxSpawnerSObj.ContentBoxes);
				boxes.AddRange(boxSpawnerSObj.ContainerBoxes);
			}
			boxes.Add(_gameSettings.OneBox);
			loadingTasks.AddRange(_boxControllerFactory.PreloadAddressableAssets(boxes.Distinct().ToArray()));
			loadingTasks.AddRange(_sfxFactory.PreloadAddressableAssets());
			loadingTasks.Add(_boxGroundedDustControllerFactory.PreloadAddressableAssets());
			loadingTasks.Add(_boxRemovedDustControllerFactory.PreloadAddressableAssets());
			loadingTasks.Add(_coinControllerFactory.PreloadAddressableAssets());
			_startupUiController.SetLoadingProgress(0);
			while(true)
			{
				var completed = GetCompletedLoadingTasks();
				_startupUiController.SetLoadingProgress(Mathf.RoundToInt((float)completed / (float)loadingTasks.Count * 100));
				if(completed == loadingTasks.Count)
					break;
				await Task.Yield();
			}

			_gameStateMachine.NextState(GameState.ReadyToPlay);

			int GetCompletedLoadingTasks()
			{
				var result = 0;
				foreach(var task in loadingTasks)
					if(task.IsCompleted)
						result++;
				return result;
			}
		}
	}
}
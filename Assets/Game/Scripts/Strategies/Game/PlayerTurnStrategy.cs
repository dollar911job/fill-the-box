﻿using System.Collections.Generic;
using System.Linq;
using Game.Controllers;
using Game.Controllers.UI;
using Game.ScriptableObjects;
using Game.Services;
using Game.StateMachine;
using UnityEngine;

namespace Game.Strategies
{
	public sealed class PlayerTurnStrategy : BaseStrategy
	{
		private const string _goodMessage = "GOOD";
		private const string _perfectMessage = "PERFECT";

		private readonly GameDataService _gameDataService;
		private readonly GameDataService.Settings _gameSettings;
		private readonly BoxScriptableObject.Settings _boxSettings;
		private readonly GameStateMachine _gameStateMachine;
		private readonly BoxController.Factory _boxFactory;
		private readonly InputService _inputService;
		private readonly GameUiController _gameUiController;
		private readonly VfxService _vfxService;
		private readonly SfxService _sfxService;

		private BoxController _originBoxController;
		private BoxController _draggedBoxController;

		public PlayerTurnStrategy(
			GameDataService gameDataService, 
			GameDataService.Settings gameSettings,
			BoxScriptableObject.Settings boxSettings,
			GameStateMachine gameStateMachine,
			BoxController.Factory boxFactory, 
			InputService inputService,
			GameUiController gameUiController,
			VfxService vfxService,
			SfxService sfxService)
		{
			_gameDataService = gameDataService;
			_gameSettings = gameSettings;
			_boxSettings = boxSettings;
			_gameStateMachine = gameStateMachine;
			_boxFactory = boxFactory;
			_inputService = inputService;
			_gameUiController = gameUiController;
			_vfxService = vfxService;
			_sfxService = sfxService;
		}

		public override void Start()
		{
			SubscribeEvents();
			_gameUiController.ToggleDestroyBoxButton(true);
			foreach(var box in _gameDataService.boxes.Keys)
				box.SetTrailEnabled(false);
		}

		public override void Finish()
		{
			foreach(var box in _gameDataService.boxes.Keys)
				box.SetTrailEnabled(true);
		}

		private void SubscribeEvents()
		{
			_inputService.DragBegin += OnDragBegin;
			_inputService.Dragged += OnDragged;
			_inputService.DragEnd += OnDragEnd;
			_gameUiController.UseDestroyBox += OnUseDestroyBox;
			_gameUiController.AddOneBoxes += OnAddOneBoxes;
			_gameUiController.Surrender += OnSurrender;
		}

		private void UnsubscribeEvents()
		{
			_inputService.DragBegin -= OnDragBegin;
			_inputService.Dragged -= OnDragged;
			_inputService.DragEnd -= OnDragEnd;
			_gameUiController.UseDestroyBox -= OnUseDestroyBox;
			_gameUiController.AddOneBoxes -= OnAddOneBoxes;
			_gameUiController.Surrender -= OnSurrender;
		}

		private void OnDragBegin(BoxController boxController, Vector3 position)
		{
			var originBoxController = _gameDataService.boxes.First((item) => item.Key == boxController).Key;
			if(_gameDataService.contentInContainerBoxes.ContainsKey(originBoxController))
				return;

			var boxPos = _gameDataService.boxes[originBoxController];
			foreach(var cellPos in originBoxController.cells)
				if((boxPos + cellPos).y >= _gameSettings.gameZoneHeight)
					return;

			_originBoxController = originBoxController;
			_draggedBoxController = UnityEngine.Object.Instantiate(boxController);
			_draggedBoxController.transform.position = new Vector3(position.x, position.y, _boxSettings.BoxMoveUpPositionZ);
			_originBoxController.Dragged(true);
			foreach(var kvp in _gameDataService.contentInContainerBoxes)
			{
				if(kvp.Value.Item1 == _originBoxController)
					kvp.Key.Dragged(true);
			}
			_sfxService.PlayAsync(SfxService.Sound.BoxUp);
		}

		private void OnDragged(Vector3 position)
		{
			if(_draggedBoxController != null)
				_draggedBoxController.transform.position = new Vector3(position.x, position.y, _boxSettings.BoxMoveUpPositionZ);
		}

		private void OnDragEnd()
		{
			if(_originBoxController == null)
				return;

			TryMoveBox();
			RemoveFullContainers();

			if(_draggedBoxController != null)
				UnityEngine.Object.Destroy(_draggedBoxController.gameObject);
			_originBoxController.Dragged(false);
			foreach(var kvp in _gameDataService.contentInContainerBoxes)
			{
				if(kvp.Value.Item1 == _originBoxController)
					kvp.Key.Dragged(false);
			}
			_originBoxController = null;

			UnsubscribeEvents();
			_gameStateMachine.NextState(GameState.UpdateGameField);
		}

		private void OnUseDestroyBox()
		{
			UnsubscribeEvents();
			_gameStateMachine.NextState(GameState.UseDestroyBoxBooster);
		}

		private void OnAddOneBoxes()
		{
			UnsubscribeEvents();
			_gameStateMachine.NextState(GameState.UseAddOneBoxesBooster);
		}

		private void OnSurrender()
		{
			UnsubscribeEvents();
			_gameStateMachine.NextState(GameState.PreGameOver);
		}

		private void TryMoveBox()
		{
			if(_originBoxController == null)
				return;

			var dropCell = GetDropCell();
			if(dropCell.y + _originBoxController.size.y > _gameSettings.gameZoneHeight)
				return;

			var boxCells = _originBoxController.cells.Select(cell => cell + dropCell);
			var fieldInfo = _gameDataService.GetFieldInfo(_originBoxController);

			if(fieldInfo.freeCells.Intersect(boxCells).Count() == boxCells.Count())
			{
				_gameDataService.boxes[_originBoxController] = dropCell;
				_originBoxController.transform.position = new Vector2(dropCell.x - _gameDataService.width / 2f + _originBoxController.size.x / 2f, dropCell.y + _originBoxController.size.y / 2f);

			}
			else if(_originBoxController.type == BoxController.Type.Content)
			{
				foreach(var kvp in fieldInfo.freeContainersCells)
				{
					var freeContainerCells = kvp.Value.Select(cell => cell + _gameDataService.boxes[kvp.Key]);

					if(freeContainerCells.Intersect(boxCells).Count() == boxCells.Count())
					{
						_gameDataService.contentInContainerBoxes.Add(_originBoxController, (kvp.Key, dropCell - _gameDataService.boxes[kvp.Key]));
						_originBoxController.CollidersSetActive(false);
						_gameDataService.boxes[_originBoxController] = dropCell;
						_originBoxController.transform.position = new Vector2(dropCell.x - _gameDataService.width / 2f + _originBoxController.size.x / 2f, dropCell.y + _originBoxController.size.y / 2f);
						_originBoxController.transform.parent = kvp.Key.transform;
						_originBoxController.MoveToContainer();
					}
				}
			}
			_sfxService.PlayAsync(SfxService.Sound.BoxDown);
		}

		private void RemoveFullContainers()
		{
			var cellsCountInContainers = new Dictionary<BoxController, int>();
			foreach(var kvp in _gameDataService.contentInContainerBoxes)
			{
				if(!cellsCountInContainers.ContainsKey(kvp.Value.Item1))
					cellsCountInContainers.Add(kvp.Value.Item1, kvp.Value.Item1.cells.Length);

				cellsCountInContainers[kvp.Value.Item1] -= kvp.Key.cells.Length;
			}

			foreach(var kvp in cellsCountInContainers)
			{
				if(kvp.Value == 0)
				{
					var score = kvp.Key.cells.Length;
					if(IsSameColor(kvp.Key))
					{
						_vfxService.ShowCongratulationMessage(_perfectMessage);
						score *= _gameSettings.SameColorMultiplier;
					}
					else
						_vfxService.ShowCongratulationMessage(_goodMessage);

					_gameDataService.score.value += score;

					foreach(var content in _gameDataService.contentInContainerBoxes.Keys.ToArray())
					{
						if(_gameDataService.contentInContainerBoxes[content].Item1 == kvp.Key)
						{
							content.transform.parent = kvp.Key.transform.parent;
							_boxFactory.Release(content);
							_gameDataService.contentInContainerBoxes.Remove(content);
							_gameDataService.boxes.Remove(content);
						}
					}

					var removedCells = new Vector2Int[kvp.Key.cells.Length];
					for(var n = 0; n < kvp.Key.cells.Length; n++)
						removedCells[n] = kvp.Key.cells[n] + _gameDataService.boxes[kvp.Key];
					_vfxService.ShowBoxRemovedDust(removedCells);
					_vfxService.ShowMovedCoins(removedCells);
					_sfxService.PlayAsync(SfxService.Sound.Coins);

					_boxFactory.Release(kvp.Key);
					_gameDataService.boxes.Remove(kvp.Key);
				}
			}
		}

		private bool IsSameColor(BoxController container)
		{
			var boxColors = new List<Color>();
            foreach (var contentInContainerBox in _gameDataService.contentInContainerBoxes)
            {
				if(contentInContainerBox.Value.Item1 == container)
					boxColors.Add(contentInContainerBox.Key.BaseColor);
			}

			return boxColors.Distinct().Count() == 1;
		}

		private Vector2Int GetDropCell()
		{
			var position = new Vector2(_draggedBoxController.transform.position.x + _gameDataService.width / 2f, _draggedBoxController.transform.position.y) - ((Vector2)_originBoxController.size / 2f);
			return Vector2Int.RoundToInt(position);
		}
	}
}
﻿using Game.Controllers.UI;
using Game.Services;
using Game.StateMachine;

namespace Game.Strategies
{
	public sealed class PreGameOverStrategy : BaseStrategy
	{
		private readonly ScreenService _screenService;
		private readonly GameStateMachine _gameStateMachine;

		private PreGameOverScreenUiController _preGameOverScreen;

		public PreGameOverStrategy(
			ScreenService screenService,
			GameStateMachine gameStateMachine)
		{
			_screenService = screenService;
			_gameStateMachine = gameStateMachine;
		}

		public override async void Start()
		{
		 	_preGameOverScreen = await _screenService.GetPreGameOverScreen();
			_preGameOverScreen.gameObject.SetActive(true);
			SubscribeEvents();
		}

		private void SubscribeEvents()
		{
			_preGameOverScreen.AddOneBoxes += OnAddOneBoxes;
			_preGameOverScreen.UseDestroyBox += OnUseDestroyBox;
			_preGameOverScreen.Back += OnBack;
			_preGameOverScreen.GiveUp += OnGiveUp;
		}

		private void UnsubscribeEvents()
		{
			_preGameOverScreen.AddOneBoxes -= OnAddOneBoxes;
			_preGameOverScreen.UseDestroyBox -= OnUseDestroyBox;
			_preGameOverScreen.Back -= OnBack;
			_preGameOverScreen.GiveUp -= OnGiveUp;
		}

		private void OnAddOneBoxes()
		{
			UnsubscribeEvents();
			_preGameOverScreen.gameObject.SetActive(false);
			_gameStateMachine.NextState(GameState.UseAddOneBoxesBooster);
		}

		private void OnUseDestroyBox()
		{
			UnsubscribeEvents();
			_preGameOverScreen.gameObject.SetActive(false);
			_gameStateMachine.NextState(GameState.UseDestroyBoxBooster);
		}

		private void OnBack()
		{
			UnsubscribeEvents();
			_preGameOverScreen.gameObject.SetActive(false);
			_gameStateMachine.NextState(GameState.PlayerTurn);
		}

		private void OnGiveUp()
		{
			UnsubscribeEvents();
			_preGameOverScreen.gameObject.SetActive(false);
			_gameStateMachine.NextState(GameState.GameOver);
		}
	}
}
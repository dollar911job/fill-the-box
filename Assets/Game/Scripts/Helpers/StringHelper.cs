﻿using UnityEngine;

namespace Game.Helpers
{
	public static class StringHelper
	{
		/// <summary>Wrapps message into color tag</summary>
		public static string SetColor(this string message, Color color) =>
			$"<color=#{ColorUtility.ToHtmlStringRGB(color)}>{message}</color>";
	}
}
﻿using UnityEngine;

namespace Game.Helpers
{
	public static class ColorHelper
	{
		public static Color AddColorByAlpha(this Color baseColor, Color color)
		{
			return new Color(MixChannels(baseColor.r, color.r), MixChannels(baseColor.g, color.g), MixChannels(baseColor.b, color.b), baseColor.a);

			float  MixChannels(float baseValue, float addValue) =>
				(baseValue + addValue * color.a) / (1f + color.a);
		}
	}
}
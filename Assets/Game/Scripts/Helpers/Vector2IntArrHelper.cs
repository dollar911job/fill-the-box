﻿using System.Linq;
using UnityEngine;

namespace Game.Helpers
{
	public static class Vector2IntArrHelper
	{
		public static Vector2Int GetSize(this Vector2Int[] cells)
		{
			(var min, var max) = GetMinAndMax(cells);

			return max - min + Vector2Int.one;
		}

		public static RectInt GetRect(this Vector2Int[] cells)
		{
			(var min, var max) = GetMinAndMax(cells);
			return new RectInt(min, max - min);
		}

		public static Vector2Int[] Turn90(this Vector2Int[] cells, int turnsCount = 1)
		{
			if(turnsCount % 4 == 0)
				return cells;

			var rect = GetRect(cells);
			var max = Mathf.Max(rect.width + 1, rect.height + 1);
			var matrix = new bool[max, max];
			for(var x = 0; x < max; x++)
			{
				for(var y = 0; y < max; y++)
				{
					matrix[x, y] = cells.Contains(new Vector2Int(x - rect.x, y - rect.y));
				}
			}

			for(var t = 0; t < turnsCount; t++)
			{
				for(var i = 0; i < max; i++)
				{
					for(var j = 0; j < i; j++)
					{
						(matrix[j, i], matrix[i, j]) = (matrix[i, j], matrix[j, i]);
					}
				}

				for(var i = 0; i < max; i++)
				{
					for(var j = 0; j < max / 2; j++)
					{
						(matrix[i, max - j - 1], matrix[i, j]) = (matrix[i, j], matrix[i, max - j - 1]);
					}
				}
			}

			var n = 0;
			var result = new Vector2Int[cells.Length];
			for(var x = 0; x < max; x++)
			{
				for(var y = 0; y < max; y++)
				{
					if(matrix[x, y])
					{
						result[n] = new Vector2Int(x, y);
						n++;
					}
				}
			}

			return Trim(result);
		}

		public static Vector2Int[] MirrorY(this Vector2Int[] cells)
		{
			var result = new Vector2Int[cells.Length];
			for(var n = 0; n < cells.Length; n++)
			{
				result[n] = new Vector2Int(cells[n].x * -1, cells[n].y);
			}
			return Trim(result);
		}

		private static Vector2Int[] Trim(Vector2Int[] cells)
		{
			var rect = GetRect(cells);
			for(var n = 0; n < cells.Length; n++)
				cells[n] = new Vector2Int(cells[n].x - rect.x, cells[n].y - rect.y);

			return cells;
		}

		private static (Vector2Int min, Vector2Int max) GetMinAndMax(Vector2Int[] cells)
		{
			var minCell = cells[0];
			var maxCell = cells[0];

			foreach(var cell in cells)
			{
				if(minCell.x > cell.x)
					minCell = new Vector2Int(cell.x, minCell.y);

				if(minCell.y > cell.y)
					minCell = new Vector2Int(minCell.x, cell.y);

				if(maxCell.x < cell.x)
					maxCell = new Vector2Int(cell.x, maxCell.y);

				if(maxCell.y < cell.y)
					maxCell = new Vector2Int(maxCell.x, cell.y);
			}

			return (minCell, maxCell);
		}
	}
}
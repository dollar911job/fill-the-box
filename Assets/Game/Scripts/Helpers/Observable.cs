﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Game.Helpers
{
	public class Observable<T>
	{
		public event Action<T> Changed;

		private T _value;
		public T value
		{
			get => _value;
			set
			{
				_value = value;
				Changed.Invoke(_value);
			}
		}
	}
}
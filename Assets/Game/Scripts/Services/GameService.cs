﻿using System;
using System.Collections.Generic;
using Game.Controllers.UI;
using Game.StateMachine;
using Game.Strategies;
using Zenject;

namespace Game.Services
{
	public class GameService : IInitializable, IDisposable
	{
		private readonly GameStateMachine _gameStateMachine;
		private readonly GameUiController _gameUiController;
		private readonly GameDataService _gameDataService;
		private readonly Dictionary<GameState, BaseStrategy> _strategies;

		public GameService(
			GameStateMachine gameStateMachine,
			GameUiController gameUiController,
			GameDataService gameDataService,
			StartGameStrategy startGameStrategy,
			UpdateGameFieldStrategy updateGameFieldStrategy,
			PlayerTurnStrategy playerTurnStrategy,
			UseDestroyBoxBoosterStrategy useDestroyBoxBoosterStrategy,
			UseAddOneBoxesBoosterStrategy useAddOneBoxesBoosterStrategy,
			PreGameOverStrategy preGameOverStrategy,
			GameOverStrategy gameOverStrategy)
		{
			_gameStateMachine = gameStateMachine;
			_gameUiController = gameUiController;
			_gameDataService = gameDataService;

			_strategies = new()
			{
				{GameState.StartGame, startGameStrategy },
				{GameState.UpdateGameField, updateGameFieldStrategy },
				{GameState.PlayerTurn, playerTurnStrategy },
				{GameState.UseDestroyBoxBooster, useDestroyBoxBoosterStrategy },
				{GameState.UseAddOneBoxesBooster, useAddOneBoxesBoosterStrategy },
				{GameState.PreGameOver, preGameOverStrategy },
				{GameState.GameOver, gameOverStrategy }
			};
		}

		public void Initialize()
		{
			_gameStateMachine.EnterState += OnEnterState;
			_gameStateMachine.ExitState += OnExitState;
			_gameDataService.score.Changed += _gameUiController.SetScore;
		}

		private void OnEnterState(GameState gameState)
		{
			if(_strategies.ContainsKey(gameState))
				_strategies[gameState].Start();
		}

		private void OnExitState(GameState gameState)
		{
			if(_strategies.ContainsKey(gameState))
				_strategies[gameState].Finish();
		}

		public void Dispose()
		{
			_gameStateMachine.EnterState -= OnEnterState;
			_gameStateMachine.ExitState -= OnExitState;
			_gameDataService.score.Changed -= _gameUiController.SetScore;
		}
	}
}
﻿using System;
using System.Threading.Tasks;
using Game.Controllers.UI;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Services
{
	public class ScreenService
	{
		private readonly PreGameOverScreenUiController.Factory _preGameOverScreenFactory;
		private readonly GameOverScreenUiController.Factory _gameOverScreenFactory;

		public ScreenService(
			PreGameOverScreenUiController.Factory preGameOverScreenFactory, 
			GameOverScreenUiController.Factory gameOverScreenFactory)
		{
			_preGameOverScreenFactory = preGameOverScreenFactory;
			_gameOverScreenFactory = gameOverScreenFactory;
		}

		public async Task<PreGameOverScreenUiController> GetPreGameOverScreen() =>
			await _preGameOverScreenFactory.Create();

		public async Task<GameOverScreenUiController> GetGameOverScreen() =>
			await _gameOverScreenFactory.Create();


		[Serializable]
		public class Settings
		{
			[SerializeField]
			private AssetReference _preGameOverScreen;
			public AssetReference PreGameOverScreen => _preGameOverScreen;

			[SerializeField]
			private AssetReference _gameOverScreen;
			public AssetReference GameOverScreen => _gameOverScreen;

			[SerializeField]
			private AssetReference _questionScreen;
			public AssetReference QuestionScreen => _questionScreen;
		}
	}
}
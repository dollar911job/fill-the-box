﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Game.Controllers.UI;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Pool;
using Zenject;

namespace Game.Services
{
	public class SfxService : IInitializable, IDisposable
	{
		private const string _soundsKey = "Sounds";

		public enum Sound
		{
			Coins,
			Click,
			BoxUp,
			BoxDown,
			BoxGroundedHight,
			BoxGroundedLow
		}

		private readonly Factory _sfxFactory;
		private readonly GameUiController _gameUiController;

		private bool _enabledSounds;

		public SfxService(
			Factory sfxFactory,
			GameUiController gameUiController)
		{
			_sfxFactory = sfxFactory;
			_gameUiController = gameUiController;
		}

		public async void PlayAsync(Sound sound, float volume = 1f) =>
			await Play(sound, volume);

		public async Task Play(Sound sound, float volume = 1f)
		{
			var audioSource = await _sfxFactory.Create(sound);
			audioSource.volume = volume;
			audioSource.Play();
			while(audioSource != null && audioSource.isPlaying)
				await Task.Yield();
			_sfxFactory.Release(audioSource);
		}

		public void Initialize()
		{
			_enabledSounds = PlayerPrefs.GetInt(_soundsKey, 1) == 1;
			_gameUiController.ToggleSounds += OnToggleSounds;
			UpdateSounds();
		}

		private void UpdateSounds()
		{
			AudioListener.volume = _enabledSounds ? 1f : 0f;
			_gameUiController.ToggleSoundsButton(_enabledSounds);
		}

		public void Dispose()
		{
			_gameUiController.ToggleSounds -= OnToggleSounds;
		}

		private void OnToggleSounds()
		{
			_enabledSounds = !_enabledSounds;
			PlayerPrefs.SetInt(_soundsKey, _enabledSounds ? 1 : 0);
			UpdateSounds();
		}

		[Serializable]
		public class Settings
		{
			[Serializable]
			public struct SoundReference
			{
				public Sound sound;
				public AssetReference assetReference;
			}

			[SerializeField]
			private SoundReference[] _sounds;
			public SoundReference[] Sounds => _sounds;

			[SerializeField]
			private float _volumeVelocityCoefficient;
			public float VolumeVelocityCoefficient => _volumeVelocityCoefficient;
		}

		public class Factory : IFactory<Sound, Task<AudioSource>>
		{
			private readonly Settings _sfxSettings;

			private Transform _parent;
			private ObjectPool<AudioSource> _pool;

			public Factory(Settings sfxSettings)
			{
				_sfxSettings = sfxSettings;
			}

			public void SetParent(Transform parent) =>
				_parent = parent;

			public List<Task> PreloadAddressableAssets()
			{
				var tasks = new List<Task>();
				foreach(var soundReference in _sfxSettings.Sounds)
					tasks.Add(soundReference.assetReference.LoadAssetAsync<AudioClip>().Task);


				return tasks;
			}

			public async Task<AudioSource> Create(Sound sound)
			{
				_pool ??= new ObjectPool<AudioSource>(
					   () => {
						   var audioSource = new GameObject(nameof(AudioSource)).AddComponent<AudioSource>();
						   audioSource.transform.SetParent(_parent);
						   audioSource.playOnAwake = false;
						   return audioSource;
					   }, 
						  (audioSource) => audioSource.gameObject.SetActive(true), 
						   (audioSource) => {
							   if(audioSource != null)
							   {
								   audioSource.clip = null;
								   audioSource.gameObject.SetActive(false);
							   }
						   });


				var audioSource = _pool.Get();
				foreach(var soundReference in _sfxSettings.Sounds)
					if(soundReference.sound == sound)
					{
						if(!soundReference.assetReference.IsValid())
							await soundReference.assetReference.LoadAssetAsync<AudioClip>().Task;

						audioSource.clip = (AudioClip)soundReference.assetReference.Asset;
					}
				return audioSource;
			}

			public void Release(AudioSource audioSource) =>
				_pool.Release(audioSource);

			public void Dispose()
			{
				foreach(var soundReference in _sfxSettings.Sounds)
					soundReference.assetReference.ReleaseAsset();
				_pool.Clear();
			}
		}
	}
}
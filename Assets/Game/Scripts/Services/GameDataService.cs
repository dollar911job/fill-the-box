﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers;
using Game.Entities;
using Game.Helpers;
using Game.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace Game.Services
{
	public class GameDataService : IInitializable
	{
		public struct FieldInfo
		{
			public List<Vector2Int> contentCells;
			public Dictionary<BoxController, List<Vector2Int>> containersCells;
			public Dictionary<BoxController, List<Vector2Int>> fullContainersCells;

			public List<Vector2Int> freeCells;
			public Dictionary<BoxController, List<Vector2Int>> freeContainersCells;
		}

		public readonly Observable<int> score;
		public readonly Dictionary<BoxController, Vector2Int> boxes = new();
		public readonly Dictionary<BoxController, (BoxController, Vector2Int)> contentInContainerBoxes = new();
		public readonly List<BoxSpawnerEntity> boxSpawners = new();

		public int width { get; private set; }
		
		private readonly Settings _settings;
		private readonly BoxSpawnerEntity.Factory _boxSpawnerFactory;
		private readonly BoxController.Factory _boxFactory;	

		public GameDataService(
			Settings settings,
			BoxSpawnerEntity.Factory boxSpawnerFactory,
			BoxController.Factory boxFactory)
		{
			_settings = settings;
			_boxSpawnerFactory = boxSpawnerFactory;
			_boxFactory = boxFactory;

			score = new();
		}

		public void Initialize()
		{
			width = 0;
			boxSpawners.Clear();
			boxes.Clear();
			contentInContainerBoxes.Clear();

			foreach(var boxSpawnerSo in _settings.BoxSpawners)
			{
				width += boxSpawnerSo.Width;
				boxSpawners.Add(_boxSpawnerFactory.Create(boxSpawnerSo));
			}
		}

		public void Clear()
		{
			foreach(var box in boxes.Keys)
				_boxFactory.Release(box);

			boxes.Clear();
			score.value = 0;
		}

		public bool CheckForVoid(RectInt zone)
		{
			foreach(var box in boxes)
			{
				foreach(var cell in box.Key.cells)
				{
					var pos = box.Value + cell;
					if(pos.x >= zone.x && pos.x < zone.x + zone.width && pos.y >= zone.y && pos.y < zone.y + zone.height)
						return false;
				}
			}

			return true;
		}

		public FieldInfo GetFieldInfo(BoxController ignoreBox) =>
			GetFieldInfo(new BoxController[] { ignoreBox });

		public FieldInfo GetFieldInfo(BoxController[] ignoreBoxes = null)
		{
			var fieldInfo = new FieldInfo()
			{
				freeCells = new(),
				contentCells = new(),
				freeContainersCells = new(),
				fullContainersCells = new(),
				containersCells = new()
			};

			foreach(var box in boxes)
			{
				if(ignoreBoxes != null && ignoreBoxes.Contains( box.Key))
					continue;

				if(box.Key.type == BoxController.Type.Content)
				{
					if(contentInContainerBoxes.ContainsKey(box.Key))
					{
						var container = contentInContainerBoxes[box.Key].Item1;
						var offsetInContainer = contentInContainerBoxes[box.Key].Item2;
						if(!fieldInfo.fullContainersCells.ContainsKey(container))
							fieldInfo.fullContainersCells.Add(container, new List<Vector2Int>());

						foreach(var cell in box.Key.cells)
							fieldInfo.fullContainersCells[container].Add(cell + offsetInContainer);
					}
					else
					{
						foreach(var cell in box.Key.cells)
							fieldInfo.contentCells.Add(cell + box.Value);
					}
				}
				else
				{
					fieldInfo.containersCells.Add(box.Key, box.Key.cells.ToList());
				}
			}

			foreach(var containerCell in fieldInfo.containersCells)
			{
				if(fieldInfo.fullContainersCells.ContainsKey(containerCell.Key))
					fieldInfo.freeContainersCells.Add(containerCell.Key, containerCell.Value.Except(fieldInfo.fullContainersCells[containerCell.Key]).ToList());
				else
					fieldInfo.freeContainersCells.Add(containerCell.Key, containerCell.Value);
			}

			for(var y = 0; y < _settings.gameZoneHeight; y++)
			{
				for(var x = 0; x < width; x++)
				{
					var cell = new Vector2Int(x, y);
					if(fieldInfo.contentCells.Contains(cell))
						continue;

					var find = false;
					foreach(var containerCellsKvp in fieldInfo.containersCells)
					{
						if(containerCellsKvp.Value.Contains(cell - boxes[containerCellsKvp.Key]))
						{
							find = true;
							break;
						}
					}
					if(find)
						continue;

					fieldInfo.freeCells.Add(cell);
				}
			}

			return fieldInfo;
		}

		[Serializable]
		public class Settings
		{
			[SerializeField]
			private BoxSpawnerScriptableObject[] _boxSpawners;
			public BoxSpawnerScriptableObject[] BoxSpawners => _boxSpawners;

			[SerializeField]
			private Color[] _boxColors;
			public Color[] BoxColors => _boxColors;

			[SerializeField]
			private int _sameColorMultiplier;
			public int SameColorMultiplier => _sameColorMultiplier;

			[SerializeField]
			private BoxScriptableObject _oneBox;
			public BoxScriptableObject OneBox => _oneBox;

			[SerializeField]
			private int _spawnZoneHeight;
			public int spawnZoneHeight => _spawnZoneHeight;

			[SerializeField]
			private int _previewZoneHeight;
			public int previewZoneHeight => _previewZoneHeight;

			[SerializeField]
			private int _gameZoneHeight;
			public int gameZoneHeight => _gameZoneHeight;
		}
	}
}
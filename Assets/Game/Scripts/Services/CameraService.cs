﻿using UnityEngine;

namespace Game.Services
{
	public class CameraService
	{
		private const float _bottomMargin = 1f;
		private readonly GameDataService _gameDataService;

		public CameraService(GameDataService gameDataService)
		{
			_gameDataService = gameDataService;
		}

		public void FitToWidth()
		{
			var camera = Camera.main;
			var unitPerPixel = (float)_gameDataService.width / Screen.width;
			camera.orthographicSize = 0.5f * Screen.height * unitPerPixel;
			camera.transform.position = new Vector3(camera.transform.position.x, Screen.height * unitPerPixel / 2f - Mathf.Sin(camera.transform.rotation.eulerAngles.x * 2 * Mathf.PI / 360f) * camera.transform.position.z - _bottomMargin, camera.transform.position.z);
		}
	}
}
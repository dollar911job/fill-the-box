﻿using System;
using Game.Controllers;
using UnityEngine;
using Zenject;

namespace Game.Services
{
	public class InputService : IInitializable, IDisposable
	{
		private const float _dragZCoord = -1f;

		public event Action<BoxController> Pressed;
		public event Action<BoxController, Vector3> DragBegin;
		public event Action<Vector3> Dragged;
		public event Action DragEnd;

		private readonly InputEventController _inputEventController;

		private Camera _camera;
		private Plane _plane;

		public InputService(InputEventController dragEventController)
		{
			_inputEventController = dragEventController;
		}

		public void Initialize()
		{
			_camera = Camera.main;
			_plane = new Plane(Vector3.forward, new Vector3(0f, 0f, _dragZCoord));
			_inputEventController.DragBegin += OnDragBegin;
			_inputEventController.Dragged += OnDragged;
			_inputEventController.DragEnd += OnDragEnd;
		}

		private void OnDragBegin(Vector2 position)
		{
			var ray = _camera.ScreenPointToRay(position);

			if(Physics.Raycast(ray, out var hit))
			{
				var boxController = hit.collider.GetComponentInParent<BoxController>();
				if(boxController != null)
				{
					if(_plane.Raycast(ray, out var dist))
					{
						Pressed?.Invoke(boxController);
						DragBegin?.Invoke(boxController, ray.GetPoint(dist));
					}
				}
			}
		}

		private void OnDragged(Vector2 position)
		{
			var ray = _camera.ScreenPointToRay(position);
			if(_plane.Raycast(ray, out var dist))
				Dragged?.Invoke(ray.GetPoint(dist));
		}

		private void OnDragEnd()
		{
			DragEnd?.Invoke();
		}

		public void Dispose()
		{
			_inputEventController.DragBegin -= OnDragBegin;
			_inputEventController.Dragged -= OnDragged;
			_inputEventController.DragEnd -= OnDragEnd;
		}
	}
}
﻿using System.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace Game.Services
{
	public class SceneService
	{
		private const int _gameSceneBuildIndex = 1;

		public async Task ActivateGameScene()
		{
			var asyncOp = SceneManager.LoadSceneAsync(_gameSceneBuildIndex);
			while(!asyncOp.isDone)
				await Task.Yield();
		}
	}
}
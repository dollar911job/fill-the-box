﻿using System;
using Game.Controllers;
using Game.Controllers.UI;
using Game.ScriptableObjects;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace Game.Services
{
	public class VfxService : IInitializable
	{
		private const float _halfCellDistance = 0.5f;

		private readonly BoxGroundedDustController.Factory _boxGroundedDustFactory;
		private readonly BoxRemovedDustController.Factory _boxRemovedDustFactory;
		private readonly CoinController.Factory _coinControllerFactory;
		private readonly GameDataService _gameDataService;
		private readonly BoxScriptableObject.Settings _boxSettings;
		private readonly GameUiController _gameUiController;
		private readonly Settings _settings;

		private Camera _camera;
		private Vector2 _coinsTargetToMove;

		public VfxService(
			BoxGroundedDustController.Factory boxGroundedDustFactory,
			BoxRemovedDustController.Factory boxRemovedDustFactory,
			CoinController.Factory coinControllerFactory,
			GameDataService gameDataService, 
			BoxScriptableObject.Settings boxSettings,
			GameUiController gameUiController,
			Settings settings)
		{
			_boxGroundedDustFactory = boxGroundedDustFactory;
			_boxRemovedDustFactory = boxRemovedDustFactory;
			_coinControllerFactory = coinControllerFactory;
			_gameDataService = gameDataService;
			_boxSettings = boxSettings;
			_gameUiController = gameUiController;
			_settings = settings;
		}

		public void Initialize()
		{
			_camera = Camera.main;
		}

		public void SetCoinsTargetToMove(Vector2 target) =>
			_coinsTargetToMove = target;

		public void ShowBoxGroundedDust(Vector2Int[] cells)
		{
			foreach(var cell in cells)
				ShowBoxGroundedDustInCell(cell);

			async void ShowBoxGroundedDustInCell(Vector2Int cell)
			{
				var dust = await _boxGroundedDustFactory.Create();
				dust.transform.position = new Vector3(cell.x - _gameDataService.width / 2f, cell.y, _boxSettings.BoxInFieldPositionZ);
				await dust.Play();
				_boxGroundedDustFactory.Release(dust);
			}
		}

		public void ShowBoxRemovedDust(Vector2Int[] cells)
		{
			foreach(var cell in cells)
				ShowBoxRemovedDustInCell(cell);

			async void ShowBoxRemovedDustInCell(Vector2Int cell)
			{
				var dust = await _boxRemovedDustFactory.Create();
				dust.transform.position = new Vector3(cell.x - _gameDataService.width / 2f, cell.y, _boxSettings.BoxInFieldPositionZ);
				await dust.Play();
				_boxRemovedDustFactory.Release(dust);
			}
		}

		public void ShowMovedCoins(Vector2Int[] cells)
		{
			foreach(var cell in cells)
				ShowMovedCoinInCell(cell);

			async void ShowMovedCoinInCell(Vector2Int cell)
			{
				var cellPosition = new Vector3(cell.x - _gameDataService.width / 2f + _halfCellDistance, cell.y + _halfCellDistance, _boxSettings.BoxInFieldPositionZ);
				var screenPosition = _camera.WorldToScreenPoint(cellPosition);
				var coin = await _coinControllerFactory.Create();
				coin.transform.position = screenPosition;
				await coin.Move(_coinsTargetToMove);
				_coinControllerFactory.Release(coin);
			}
		}

		public void ShowCongratulationMessage(string text) =>
			_gameUiController.ShowCongratulationMessage(text, _settings.congratulationMessageShowTime, _settings.congratulationMessageFadeAnimationTime);

		[Serializable]
		public class Settings
		{
			[SerializeField]
			private float _minBoxVelocityForGroundedDust;
			public float MinBoxVelocityForGroundedDust => _minBoxVelocityForGroundedDust;

			[SerializeField]
			private AssetReference _boxGroundedDust;
			public AssetReference BoxGroundedDustReference => _boxGroundedDust;

			[SerializeField]
			private AssetReference _boxRemovedDust;
			public AssetReference BoxRemovedDustReference => _boxRemovedDust;

			[SerializeField]
			private AssetReference _coin;
			public AssetReference CoinReference => _coin;

			[SerializeField]
			private float _congratulationMessageShowTime;
			public float congratulationMessageShowTime => _congratulationMessageShowTime;

			[SerializeField]
			private float _congratulationMessageFadeAnimationTime;
			public float congratulationMessageFadeAnimationTime => _congratulationMessageFadeAnimationTime;
		}
	}
}
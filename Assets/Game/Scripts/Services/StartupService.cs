﻿using System;
using System.Collections.Generic;
using Game.StateMachine;
using Game.Strategies;
using Zenject;

namespace Game.Services
{
	public class StartupService : IInitializable, IDisposable
	{
		private readonly GameStateMachine _gameStateMachine;
		private readonly Dictionary<GameState, BaseStrategy> _strategies;

		public StartupService(
			GameStateMachine gameStateMachine,
			StartupAppStrategy startupAppStrategy,
			ReadyToPlayStrategy readyToPlayStrategy,
			LoadingGameSceneStrategy loadingGameSceneStrategy)
		{
			_gameStateMachine = gameStateMachine;

			_strategies = new()
			{
				{GameState.StartupApp, startupAppStrategy },
				{GameState.ReadyToPlay, readyToPlayStrategy },
				{GameState.LoadingGameScene, loadingGameSceneStrategy }
				
			};
		}

		public void Initialize()
		{
			_gameStateMachine.EnterState += OnEnterState;
			_gameStateMachine.ExitState += OnExitState;
		}

		private void OnEnterState(GameState gameState)
		{
			if(_strategies.ContainsKey(gameState))
				_strategies[gameState].Start();
		}

		private void OnExitState(GameState gameState)
		{
			if(_strategies.ContainsKey(gameState))
				_strategies[gameState].Finish();
		}

		public void Dispose()
		{
			_gameStateMachine.EnterState -= OnEnterState;
			_gameStateMachine.ExitState -= OnExitState;
		}
	}
}
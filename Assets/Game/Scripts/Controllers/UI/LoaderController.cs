﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controllers
{
	public class LoaderController : MonoBehaviour
	{

		[SerializeField]
		private Image _filler;

		[SerializeField]
		private TMP_Text _value;

		public void SetValue(int percent)
		{
			_filler.fillAmount = 0.01f * percent;
			_value.text = $"{percent}%";
		}
	}
}
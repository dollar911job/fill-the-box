﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace Game.Controllers
{
	public abstract class ScreenUiFactory<T> : IFactory<Task<T>> where T : Object
	{
		private readonly DiContainer _container;

		protected AssetReference assetReference;
		private RectTransform _screensUiRoot;

		private T _cashed;

		public ScreenUiFactory(DiContainer container)
		{
			_container = container;
		}

		public void SetScreensUiRoot(RectTransform screensUiRoot) =>
			_screensUiRoot = screensUiRoot;

		public async Task<T> Create()
		{
			if(_cashed == null)
			{
				_cashed = _container.InstantiatePrefabForComponent<T>(await assetReference.LoadAssetAsync<GameObject>().Task, _screensUiRoot);
				assetReference.ReleaseAsset();
			}

			return _cashed;
		}
	}
}
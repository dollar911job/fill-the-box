﻿using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controllers.UI
{
	public class GameUiController : MonoBehaviour
	{
		private const string _progressKey = "_Progress";

		public event Action UseDestroyBox;
		public event Action CancelDestroyBox;
		public event Action AddOneBoxes;
		public event Action Surrender;
		public event Action ToggleSounds;

		[SerializeField]
		private RectTransform _safeArea;

		[SerializeField]
		private TMP_Text _scoreText;

		[SerializeField]
		private CanvasGroup _boosterButtons;

		[SerializeField]
		private Button _useDestroyBoxButton;

		[SerializeField]
		private Button _cancelDestroyBoxButton;

		[SerializeField]
		private Button _addOneBoxesButton;

		[SerializeField]
		private Button _surrenderButton;

		[SerializeField]
		private Button _soundsButton;

		[SerializeField]
		private GameObject _soundsButtonCross;

		[SerializeField]
		private TMP_Text _messageText;

		private void Awake()
		{
			_messageText.gameObject.SetActive(false);
			_useDestroyBoxButton.onClick.AddListener(() => UseDestroyBox?.Invoke());
			_cancelDestroyBoxButton.onClick.AddListener(() => CancelDestroyBox?.Invoke());
			_addOneBoxesButton.onClick.AddListener(() => AddOneBoxes?.Invoke());
			_surrenderButton.onClick.AddListener(() => Surrender?.Invoke());
			_soundsButton.onClick.AddListener(() => ToggleSounds?.Invoke());
			ToggleDestroyBoxButton(true);
			ToggleSurrenderButton(false);
			UpdateSafeArea();
		}

		private void OnDestroy()
		{
			_useDestroyBoxButton.onClick.RemoveAllListeners();
			_cancelDestroyBoxButton.onClick.RemoveAllListeners();
			_addOneBoxesButton.onClick.RemoveAllListeners();
			_surrenderButton.onClick.RemoveAllListeners();
		}

		public void SetScore(int value) =>
			_scoreText.text = value.ToString();

		public void ToggleDestroyBoxButton(bool value)
		{
			_useDestroyBoxButton.gameObject.SetActive(value);
			_cancelDestroyBoxButton.gameObject.SetActive(!value);
		}

		public void ToggleSurrenderButton(bool value) =>
			_surrenderButton.gameObject.SetActive(value);

		public void ToggleSoundsButton(bool value) =>
			_soundsButtonCross.SetActive(!value);

		private void UpdateSafeArea()
		{
			_safeArea.offsetMin = Screen.safeArea.min;
			_safeArea.offsetMax = new Vector2(Screen.safeArea.max.x - Screen.width, Screen.safeArea.max.y - Screen.height);
		}

		public async void ShowCongratulationMessage(string text, float showTime, float fadeAnimationTime)
		{
			_messageText.text = text;
			_messageText.gameObject.SetActive(true);
			var material = _messageText.font.material;
			material.SetFloat(_progressKey, 0f);
			await Task.Delay(TimeSpan.FromSeconds(showTime));
			var startTime = Time.time;
			var finishTime = startTime + fadeAnimationTime;
			var progress = 0f;
			while(progress < 1f)
			{
				progress = Mathf.InverseLerp(startTime, finishTime, Time.time);
				material.SetFloat(_progressKey, progress);
				await Task.Yield();
			}
			_messageText.gameObject.SetActive(false);
		}
	}
}
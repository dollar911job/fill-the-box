﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Controllers.UI
{
	public class StartupUiController : MonoBehaviour
	{
		private string _tutorialKey = "Tutorial";

		public event Action PlayButtonClicked;

		[SerializeField]
		private LoaderController _loaderController;

		[SerializeField]
		private Button _playButton;

		[SerializeField]
		private GameObject _tutorial;

		private void Start()
		{
			PlayButtonSetActive(false);
			_playButton.onClick.RemoveAllListeners();
			_playButton.onClick.AddListener(() => PlayButtonClicked?.Invoke());
			_tutorial.SetActive(PlayerPrefs.GetInt(_tutorialKey, 0) == 0);
			PlayerPrefs.SetInt(_tutorialKey, 1);
		}

		public void PlayButtonSetActive(bool value)
		{
			_playButton.gameObject.SetActive(value);
			_loaderController.gameObject.SetActive(!value);
		}

		public void SetLoadingProgress(int percent) =>
			_loaderController.SetValue(percent);

		private void OnDestroy()
		{
			_playButton.onClick.RemoveAllListeners();
		}
	}
}
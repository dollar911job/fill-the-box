﻿using System;
using Game.Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Controllers.UI
{
	public class GameOverScreenUiController : MonoBehaviour
	{
		public event Action PlayAgain;

		[SerializeField]
		private GameObject _newRecordMessage;

		[SerializeField]
		private TMP_Text _bestScoreValueText;

		[SerializeField]
		private TMP_Text _currentScoreValueText;

		[SerializeField]
		private Button _playAgainButton;

		private void Awake()
		{
			_playAgainButton.onClick.AddListener(() => PlayAgain?.Invoke());
		}

		public void Show(int best, int current, bool newRecord)
		{
			_newRecordMessage.gameObject.SetActive(newRecord);
			_bestScoreValueText.text = best.ToString();
			_currentScoreValueText.text = best.ToString();
			gameObject.SetActive(true);
		}

		public void Hide() =>
			gameObject.SetActive(false);

		private void OnDestroy()
		{
			_playAgainButton.onClick.RemoveAllListeners();
		}

		public class Factory : ScreenUiFactory<GameOverScreenUiController>
		{
			public Factory(
				DiContainer container,
				ScreenService.Settings settings) : base(container)
			{
				assetReference = settings.GameOverScreen;
			}
		}
	}
}
﻿using System;
using System.Threading.Tasks;
using Game.Services;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Zenject;

namespace Game.Controllers.UI
{
	public class PreGameOverScreenUiController : MonoBehaviour
	{
		public event Action Back;
		public event Action UseDestroyBox;
		public event Action AddOneBoxes;
		public event Action GiveUp;

		[SerializeField]
		private TMP_Text _titleText;

		[SerializeField]
		private Button _useDestroyBoxButton;

		[SerializeField]
		private Button _addOneBoxesButton;

		[SerializeField]
		private Button _backButton;

		[SerializeField]
		private Button _giveUpButton;

		private void Awake()
		{
			_backButton.onClick.AddListener(() => Back?.Invoke());
			_useDestroyBoxButton.onClick.AddListener(() => UseDestroyBox?.Invoke());
			_addOneBoxesButton.onClick.AddListener(() => AddOneBoxes?.Invoke());
			_giveUpButton.onClick.AddListener(() => GiveUp?.Invoke());
		}

		private void OnDestroy()
		{
			_addOneBoxesButton.onClick.RemoveAllListeners();
			_useDestroyBoxButton.onClick.RemoveAllListeners();
			_backButton.onClick.RemoveAllListeners();
			_giveUpButton.onClick.RemoveAllListeners();
		}

		public class Factory : ScreenUiFactory<PreGameOverScreenUiController>
		{
			public Factory(
				DiContainer container,
				ScreenService.Settings settings) : base(container)
			{
				assetReference = settings.PreGameOverScreen;
			}
		}
	}
}
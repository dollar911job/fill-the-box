﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Game.Helpers;
using Game.ScriptableObjects;
using Game.Services;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.Rendering;
using Zenject;

namespace Game.Controllers
{
	public class BoxController : MonoBehaviour
	{
		private const string _baseColorKey = "_BaseColor";
		private const string _shakingKeyword = "_SHAKING";

		public enum Type
		{
			Content,
			Container
		}

		public enum Rotation
		{
			Up,
			Right,
			Down,
			Left
		}

		[SerializeField]
		private MeshRenderer _content;

		[SerializeField]
		private MeshRenderer _containerL;

		[SerializeField]
		private MeshRenderer _containerR;

		[SerializeField]
		private GameObject _colliders;

		[SerializeField]
		private TrailRenderer _boxTrailVertical;

		[SerializeField]
		private TrailRenderer _boxTrailHorizontal;

		public Type type { get; private set; }
		public Rotation rotation { get; private set; }
		public bool mirrored { get; private set; }
		public Vector2Int size { get; private set; }
		public (bool x, bool y) halfCellOffset { get; private set; }
		public int poolId { get; private set; }
		public bool inContainer { get; private set; }
		public bool inPreview { get; private set; }
		public Vector2Int[] cells { get; private set; }
		public Vector2Int[] downCells { get; private set; }

		private Vector2Int[] _localCells;
		private Color _baseColor;
		public Color BaseColor => _baseColor;
		private BoxScriptableObject.Settings _settings;

		private Color _color;
		private Color _inContainerAdditionalColor;
		private Color _inPreviewAdditionalColor;

		private LocalKeyword _shakingContentKeyword;
		private LocalKeyword _shakingContainerLKeyword;
		private LocalKeyword _shakingContainerRKeyword;


		public void Initialize(int poolId, Vector2Int[] localCells, Color color, BoxScriptableObject.Settings settings)
		{
			this.poolId = poolId;
			_localCells = localCells;
			_baseColor = color;
			_settings = settings;
		}

		public void Setup()
		{
			rotation = (Rotation)UnityEngine.Random.Range(0, Enum.GetValues(typeof(Rotation)).Length);
			mirrored = UnityEngine.Random.Range(-1f, 1f) > 0f;

			size = _localCells.GetSize();
			if((int)rotation % 2 > 0)
				size = new Vector2Int(size.y, size.x);

			_boxTrailVertical.gameObject.SetActive(false);
			_boxTrailHorizontal.gameObject.SetActive(false);

			if(rotation == Rotation.Up || rotation == Rotation.Down)
			{
				_boxTrailVertical.gameObject.SetActive(true);
				_boxTrailVertical.transform.localPosition = new Vector3(0f, size.y / (rotation == Rotation.Up ? 2f : -2f) , _boxTrailVertical.transform.localPosition.z);
			}
			else
			{
				_boxTrailHorizontal.gameObject.SetActive(true);
				_boxTrailHorizontal.transform.localPosition = new Vector3(size.y / (rotation == Rotation.Left ? 2f : -2f), 0f, _boxTrailHorizontal.transform.localPosition.z);
			}

			halfCellOffset = ((size.x % 2 > 0), (size.y % 2 > 0));

			var isContent = type == Type.Content;

			gameObject.SetActive(true);

			transform.rotation = Quaternion.identity;
			_content.transform.parent.rotation = Quaternion.identity;
			_colliders.transform.rotation = Quaternion.identity;

			CollidersSetActive(true);
			_content.gameObject.SetActive(isContent);
			if(_containerL == _containerR)
				_containerL.gameObject.SetActive(!isContent);
			else
			{
				_containerL.gameObject.SetActive(!isContent && !mirrored);
				_containerR.gameObject.SetActive(!isContent && mirrored);
			}

			_content.transform.parent.Rotate(Vector3.up, mirrored && isContent ? 180 : 0);
			_colliders.transform.Rotate(Vector3.up, mirrored ? 180 : 0);

			transform.Rotate(Vector3.forward, -90f * (int)rotation);

			_color = isContent ? _baseColor : _settings.ContainerColor;
			_inContainerAdditionalColor = _settings.InContainerAdditionalColor;
			_inPreviewAdditionalColor = _settings.InPreviewAdditionalColor;
			inPreview = true;
			SetMaterial(_settings.BoxMaterial);
			UpdateColor();
			UpdateCells();

			_shakingContentKeyword = new LocalKeyword(_content.material.shader, _shakingKeyword);
			_shakingContainerLKeyword = new LocalKeyword(_containerL.material.shader, _shakingKeyword);
			_shakingContainerRKeyword = new LocalKeyword(_containerR.material.shader, _shakingKeyword);
		}

		public void SetType(Type type) =>
			this.type = type;

		public void SetInPreview(bool value) =>
			inPreview = value;

		public void UpdateColor() => 
			SetColor(inPreview ? _color.AddColorByAlpha(_inPreviewAdditionalColor) : (inContainer ? _color.AddColorByAlpha(_inContainerAdditionalColor) : _color));

		public void CollidersSetActive(bool value) =>
			_colliders.SetActive(value);

		public void MoveToContainer() => 
			inContainer = true;

		public void Dragged(bool active)
		{
			SetMaterial(active ? _settings.DragTargetBoxMaterial : _settings.BoxMaterial);
			SetColor(active ? new Color(_color.r, _color.g, _color.b, 0.5f) : (inContainer ? _color.AddColorByAlpha(_inContainerAdditionalColor) : _color));
		}

		public void SetTrailEnabled(bool value)
		{
			_boxTrailVertical.emitting = value;
			_boxTrailHorizontal.emitting = value;
		}

		private void SetColor(Color color)
		{
			_content.material.SetColor(_baseColorKey, color);
			_containerL.material.SetColor(_baseColorKey, color);
			_containerR.material.SetColor(_baseColorKey, color);
		}

		public void SetShaking(bool value)
		{
			if(value)
			{
				_content.material.EnableKeyword(_shakingContentKeyword);
				_containerL.material.EnableKeyword(_shakingContainerLKeyword);
				_containerR.material.EnableKeyword(_shakingContainerRKeyword);
			}
			else
			{
				_content.material.DisableKeyword(_shakingContentKeyword);
				_containerL.material.DisableKeyword(_shakingContainerLKeyword);
				_containerR.material.DisableKeyword(_shakingContainerRKeyword);
			}
		}

		private void SetMaterial(Material material)
		{
			_content.material = material;
			_containerL.material = material;
			_containerR.material = material;
		}

		private void UpdateCells()
		{
			cells = _localCells.ToArray();
			if(mirrored)
				cells = cells.MirrorY();
			cells = cells.Turn90((int)rotation);


			downCells = new Vector2Int[size.x];
			for(var x = 0; x < size.x; x++)
				downCells[x] = new Vector2Int(x, int.MaxValue);
			foreach(var cell in cells)
			{
				if(downCells[cell.x].y > cell.y)
					downCells[cell.x] = cell;
			}
		}

		public void Dispose()
		{
			inContainer = false;
			gameObject.SetActive(false);
		}

		public class Factory : IFactory<BoxScriptableObject, Task<BoxController>>, IDisposable
		{
			private readonly BoxScriptableObject.Settings _settings;
			private readonly GameDataService.Settings _gameSettings;

			private Dictionary<int, UnityEngine.Pool.ObjectPool<BoxController>> _pools = new();
			private BoxScriptableObject[] _boxScriptableObjects;
			private Transform _parent;

			public Factory(
				BoxScriptableObject.Settings settings,
				GameDataService.Settings gameSettings)
			{
				_settings = settings;
				_gameSettings = gameSettings;
			}

			public void SetParent(Transform parent) =>
				_parent = parent;

			public List<Task> PreloadAddressableAssets(BoxScriptableObject[] boxScriptableObjects)
			{
				var tasks = new List<Task>();
				_boxScriptableObjects = boxScriptableObjects;
				foreach(var boxScriptableObject in _boxScriptableObjects)
					tasks.Add(boxScriptableObject.BoxControllerReference.LoadAssetAsync<GameObject>().Task);
				return tasks;
			}

			public async Task<BoxController> Create(BoxScriptableObject scriptableObject)
			{
					var assetGUID = scriptableObject.BoxControllerReference.AssetGUID;

				if(!scriptableObject.BoxControllerReference.IsValid())
					await scriptableObject.BoxControllerReference.LoadAssetAsync<GameObject>().Task;

					var poolId = scriptableObject.GetInstanceID();

					if(!_pools.ContainsKey(poolId))
					{
						_pools.Add(poolId, new UnityEngine.Pool.ObjectPool<BoxController>(
							  () => {
								  var boxController = Instantiate(((GameObject)scriptableObject.BoxControllerReference.Asset).GetComponent<BoxController>());
								  boxController.Initialize(poolId, scriptableObject.Cells, GetRandomColor(), _settings);
								  return boxController;
							  },
							  actionOnRelease: (box) => box.Dispose()));
					}

					var boxController = _pools[poolId].Get();
					boxController.transform.parent = _parent;

					return boxController;
				}

			public void ClearPools()
			{
				foreach(var pool in _pools.Values)
					pool.Clear();
			}

			public void Release(BoxController boxEntity)
			{
				_pools[boxEntity.poolId].Release(boxEntity);
			}

			private Color GetRandomColor() =>
				_gameSettings.BoxColors[UnityEngine.Random.Range(0, _gameSettings.BoxColors.Length)];
			public void Dispose()
			{
				if(_boxScriptableObjects != null)
				{
					foreach(var boxScriptableObject in _boxScriptableObjects)
						boxScriptableObject.BoxControllerReference.ReleaseAsset();
				}
			}
		}
	}
}
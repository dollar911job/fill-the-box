﻿using System;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace Game.Controllers
{
	public class InputEventController : MonoBehaviour
	{
		public event Action<Vector2> DragBegin;
		public event Action<Vector2> Dragged;
		public event Action DragEnd;

		public Vector2 ScreenPosition { get; private set; }
		private bool _pressed;
		public void Position(CallbackContext callbackContext)
		{
			ScreenPosition = callbackContext.ReadValue<Vector2>();
		}

		public void Pressed(CallbackContext callbackContext)
		{
			if(callbackContext.started)
			{
				DragBegin?.Invoke(ScreenPosition);
				_pressed = true;
			}
			else if(callbackContext.canceled)
			{
				DragEnd?.Invoke();
				_pressed = false;
			}
		}

		private void Update()
		{
			if(_pressed)
				Dragged?.Invoke(ScreenPosition);
		}
	}
}
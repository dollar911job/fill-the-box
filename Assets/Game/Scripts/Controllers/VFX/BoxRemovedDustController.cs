﻿using System;
using System.Threading.Tasks;
using Game.Services;
using UnityEngine;
using UnityEngine.Pool;
using Zenject;

namespace Game.Controllers
{
	public class BoxRemovedDustController : DustController
	{
		public class Factory : IFactory<Task<BoxRemovedDustController>>, IDisposable
		{

			private readonly VfxService.Settings _vfxSettings;

			private Transform _parent;
			private ObjectPool<BoxRemovedDustController> _pool;

			public Factory(VfxService.Settings vfxSettings)
			{
				_vfxSettings = vfxSettings;
			}

			public void SetParent(Transform parent) =>
				_parent = parent;

			public async Task PreloadAddressableAssets() =>
				await _vfxSettings.BoxRemovedDustReference.LoadAssetAsync<GameObject>().Task;

			public async Task<BoxRemovedDustController> Create()
			{
				if(!_vfxSettings.BoxRemovedDustReference.IsValid())
					await PreloadAddressableAssets();

				_pool ??= new ObjectPool<BoxRemovedDustController>(
						   () => {
							   var dust = Instantiate(((GameObject)_vfxSettings.BoxRemovedDustReference.Asset).GetComponent<BoxRemovedDustController>());
							   dust.transform.parent = _parent;
							   return dust;
						   },
							 (dust) => {
								 if(dust != null)
									 dust.gameObject.SetActive(true);
							 },
							   (dust) => {
								   if(dust != null)
									   dust.gameObject.SetActive(false);
							   });
				return _pool.Get();
			}

			public void Release(BoxRemovedDustController dustController) =>
				_pool.Release(dustController);

			public void Dispose() =>
				_vfxSettings.BoxRemovedDustReference.ReleaseAsset();
		}
	}
}
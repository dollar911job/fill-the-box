﻿using System.Threading.Tasks;
using UnityEngine;

namespace Game.Controllers
{
	public abstract class DustController : MonoBehaviour
	{

		[SerializeField]
		private ParticleSystem _dust;

		public async Task Play()
		{
			_dust.Play();
			while(_dust != null && _dust.isPlaying)
				await Task.Yield();
		}
	}
}
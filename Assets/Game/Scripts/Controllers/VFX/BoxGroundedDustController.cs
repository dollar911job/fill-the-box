﻿using System;
using System.Threading.Tasks;
using Game.Services;
using UnityEngine;
using UnityEngine.Pool;
using Zenject;

namespace Game.Controllers
{
	public class BoxGroundedDustController : DustController
	{
		public class Factory : IFactory<Task<BoxGroundedDustController>>, IDisposable
		{
			private readonly VfxService.Settings _vfxSettings;

			private Transform _parent;
			private ObjectPool<BoxGroundedDustController> _pool;

			public Factory(VfxService.Settings vfxSettings)
			{
				_vfxSettings = vfxSettings;
			}

			public void SetParent(Transform parent) =>
				_parent = parent;

			public async Task PreloadAddressableAssets() =>
				await _vfxSettings.BoxGroundedDustReference.LoadAssetAsync<GameObject>().Task;

			public async Task<BoxGroundedDustController> Create()
			{
				if(!_vfxSettings.BoxGroundedDustReference.IsValid())
					await PreloadAddressableAssets();

				_pool ??= new ObjectPool<BoxGroundedDustController>(
						   () => {
							   var dust = Instantiate(((GameObject)_vfxSettings.BoxGroundedDustReference.Asset).GetComponent<BoxGroundedDustController>());
							   dust.transform.parent = _parent;
							   return dust;
						   },
							 (dust) => {
								if(dust != null)
									dust.gameObject.SetActive(true);
							 },
							   (dust) => {
								   if(dust != null)
									   dust.gameObject.SetActive(false);
							   });
				return _pool.Get();
			}

			public void Release(BoxGroundedDustController dustController) =>
				_pool.Release(dustController);

			public void Dispose() =>
				_vfxSettings.BoxGroundedDustReference.ReleaseAsset();
		}
	}
}
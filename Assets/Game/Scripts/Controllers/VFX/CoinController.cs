﻿using System;
using System.Threading.Tasks;
using DG.Tweening;
using Game.Services;
using UnityEngine;
using UnityEngine.Pool;
using Zenject;

namespace Game.Controllers
{
	public class CoinController : MonoBehaviour
	{
		[SerializeField]
		private float _acceleration;

		public async Task Move(Vector2 to)
		{
			 var startTime = Time.unscaledTime;
			var startPosition = (Vector2)transform.position;
			var path = to - startPosition;
			while(true)
			{
				var calculatedPosition = startPosition + (_acceleration * Mathf.Pow(Time.unscaledTime - startTime, 2) / 2) * path.normalized;
				var calculatedPath = calculatedPosition - startPosition;
				if(calculatedPath.sqrMagnitude < path.sqrMagnitude)
					transform.position = calculatedPosition;
				else
				{
					transform.position = to;
					break;
				}
				await Task.Yield();
			}
		}

		public class Factory : IFactory<Task<CoinController>>, IDisposable
		{
			private readonly VfxService.Settings _vfxSettings;

			private Transform _parent;
			private ObjectPool<CoinController> _pool;

			public Factory(VfxService.Settings vfxSettings)
			{
				_vfxSettings = vfxSettings;
			}

			public void SetParent(Transform parent) =>
				_parent = parent;

			public async Task PreloadAddressableAssets() =>
				await _vfxSettings.CoinReference.LoadAssetAsync<GameObject>().Task;

			public async Task<CoinController> Create()
			{
				if(!_vfxSettings.CoinReference.IsValid())
					await PreloadAddressableAssets();

				_pool ??= new ObjectPool<CoinController>(
						   () => {
							   var coin = Instantiate(((GameObject)_vfxSettings.CoinReference.Asset).GetComponent<CoinController>());
							   coin.transform.SetParent(_parent);
							   return coin;
						   },
							 (coin) => {
								 if(coin != null)
									 coin.gameObject.SetActive(true);
							 },
							   (coin) => {
								   if(coin != null)
									   coin.gameObject.SetActive(false);
							   });
				return _pool.Get();
			}

			public void Release(CoinController coinController) =>
				_pool.Release(coinController);

			public void Dispose() =>
				_vfxSettings.CoinReference.ReleaseAsset();
		}
	}
}
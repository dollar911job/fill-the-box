﻿using System;
using System.Collections.Generic;
using Game.Helpers;
using UnityEngine;
using Zenject;

namespace Game.StateMachine
{
	public interface IStateMachine<TEnum> : IInitializable
	{
		event Action<TEnum> EnterState;
		event Action<TEnum> ExitState;
		TEnum CurrentState { get; }
		TEnum PreviousState { get; }
		void NextState(TEnum state);
		void Start();
	}

	public abstract class BaseStateMachine<TEnum> : IStateMachine<TEnum> where TEnum : Enum
	{
		public event Action<TEnum> EnterState;
		public event Action<TEnum> ExitState;

		public bool debug;
		public string logKey = "StateMachine";
		public Color logColor = Color.white;

		public Dictionary<TEnum, List<TEnum>> Transitions { get; private set; }
		public TEnum CurrentState { get; private set; }
		public TEnum PreviousState { get; private set; }

		private bool _initialized;

		public abstract void Initialize();

		protected void Initialize(Dictionary<TEnum, List<TEnum>> transitions, TEnum startState)
		{
			Transitions = transitions;
			CurrentState = startState;
			PreviousState = startState;

			if(debug)
				Debug.Log(($"[{logKey}] {(_initialized ? "Reinitialized." : "Initialized.")}").SetColor(logColor));

			_initialized = true;
		}

		public void Start()
		{
			if(_initialized)
			{
				if(debug)
					Debug.Log($"[{logKey}] Started: '{CurrentState}'".SetColor(logColor));

				EnterState?.Invoke(CurrentState);
			}
			else
			{
				Debug.LogError($"[{logKey}] State machine has not been initialized and can't started.");
			}
		}

		public void NextState(TEnum state)
		{
			if(!Transitions.ContainsKey(CurrentState) || !Transitions[CurrentState].Contains(state))
				Debug.LogError($"[{logKey}] Transition '{CurrentState}' -> '{state}' not exist.");
			else
			{
				if(debug)
					Debug.Log($"[{logKey}] Transition: '{CurrentState}' -> '{state}'".SetColor(logColor));

				ExitState?.Invoke(CurrentState);
				PreviousState = CurrentState;
				CurrentState = state;
				EnterState?.Invoke(CurrentState);
			}
		}
	}
}
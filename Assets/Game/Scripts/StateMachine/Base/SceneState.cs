﻿/*using Game.StateMachines.App;
using System;
using UnityEngine.SceneManagement;

namespace Game.Base.StateMachine
{
	public abstract class SceneState : BaseState
	{
		private readonly string _sceneName;
		private readonly bool _reloading;

		protected SceneState(string sceneName, bool reloading = true)
		{
			_sceneName = sceneName;
			_reloading = reloading;
		}

		public override void Enter()
		{
			if(_reloading || SceneManager.GetActiveScene().name != _sceneName)
				SceneManager.LoadScene(_sceneName);

			base.Enter();
		}
	}
}*/
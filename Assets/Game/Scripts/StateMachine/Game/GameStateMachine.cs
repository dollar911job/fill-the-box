﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.StateMachine
{
	public enum GameState
	{
		StartupApp,
		ReadyToPlay,
		LoadingGameScene,
		StartGame,
		UpdateGameField,
		PlayerTurn,
		UseDestroyBoxBooster,
		UseAddOneBoxesBooster,
		PreGameOver,
		GameOver
	}

	public class GameStateMachine : BaseStateMachine<GameState>
	{
		public override void Initialize()
		{
//#if UNITY_EDITOR
			debug = true;
			logKey = nameof(GameStateMachine);
			logColor = Color.blue;
//#endif
			Initialize(new Dictionary<GameState, List<GameState>>() {
				{ GameState.StartupApp, new List<GameState>(){ GameState.ReadyToPlay } },
				{ GameState.ReadyToPlay, new List<GameState>(){ GameState.LoadingGameScene } },
				{ GameState.LoadingGameScene, new List<GameState>(){ GameState.StartGame } },
				{ GameState.StartGame, new List<GameState>(){ GameState.UpdateGameField } },
				{ GameState.UpdateGameField, new List<GameState>(){ GameState.PlayerTurn} },
				{ GameState.PlayerTurn, new List<GameState>(){
					GameState.UseDestroyBoxBooster, 
					GameState.UseAddOneBoxesBooster, 
					GameState.UpdateGameField, 
					GameState.PreGameOver }
				},
				{ GameState.PreGameOver, new List<GameState>(){ 
					GameState.PlayerTurn, 
					GameState.UseDestroyBoxBooster, 
					GameState.UseAddOneBoxesBooster, 
					GameState.GameOver } 
				},
				{ GameState.UseDestroyBoxBooster,  new List<GameState>(){ GameState.UpdateGameField } },
				{ GameState.UseAddOneBoxesBooster,  new List<GameState>(){ GameState.UpdateGameField } },
				{ GameState.GameOver, new List<GameState>(){ GameState.StartGame } },
			},
			GameState.StartupApp);
		}
	}
}
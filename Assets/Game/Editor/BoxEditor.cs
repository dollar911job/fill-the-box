using Game.Helpers;
using Game.ScriptableObjects;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BoxScriptableObject))]
public class BoxEditor : Editor
{
	private const float _boxSize = 30f;

	SerializedProperty _boxControllerReference;
	SerializedProperty _color;
	SerializedProperty _cells;
	
	void OnEnable()
	{
		_boxControllerReference = serializedObject.FindProperty(nameof(_boxControllerReference));
		_cells = serializedObject.FindProperty(nameof(_cells));
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.PropertyField(_boxControllerReference);
		EditorGUILayout.PropertyField(_cells);
		serializedObject.ApplyModifiedProperties();

		var lastRect = GUILayoutUtility.GetLastRect();
		var offset = new Vector2(_boxSize, lastRect.y + lastRect.height + _boxSize);

		var cells = ((BoxScriptableObject)target).Cells;

		var rect = cells.GetRect();

		foreach(var cell in cells)
		{
			var x = (cell.x - rect.x) * _boxSize + offset.x;
			var y = (rect.height - cell.y) * _boxSize + offset.y;
			EditorGUI.DrawRect(new Rect(x, y, _boxSize, _boxSize), Color.green);
		}
	}
}


